package org.limine.snapper.formats.snapper;

public enum SnapperProperty {
    // Check properties from "snapper list --columns --help"
    DESCRIPTION("description"),
    CLEANUP("cleanup"),
    DATE("date"),
    TYPE("type"),
    PRE("pre"),
    DEFAULT("default"),
    SUBVOLUME("subvolume"),
    ACTIVE("active"),
    READ_ONLY("ro"),
    USER("user"),
    PRE_NUMBER("pre_number"),
    POST_NUMBER("post_number"),
    SNAPPER_VERSION("snapper_version");

    public final String replace;

    SnapperProperty(String replace) {
        this.replace = replace;
    }

    @Override
    public String toString() {
        return this.replace;
    }
}

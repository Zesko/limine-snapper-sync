package org.limine.snapper.formats.json1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record SnapshotEntry(@JsonProperty("name") String name,
                            @JsonProperty("snapperID") SnapperEntry snapperEntry,
                            @JsonProperty("kernelEntries") List<KernelEntry> kernelEntries,
                            @JsonProperty("properties") Map<String, String> properties) {

    /**
     * Custom constructor with default value for properties
     */
    public SnapshotEntry {
        if (properties == null) {
            properties = new HashMap<>();
        }
        if (kernelEntries == null) {
            kernelEntries = new ArrayList<>();
        }
    }
}

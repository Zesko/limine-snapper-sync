package org.limine.snapper.formats.json1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LimineHistory {

    @JsonProperty("snapshotEntries")
    private final List<SnapshotEntry> snapshotEntries;
    @JsonProperty("properties")
    private final HashMap<String, String> properties;
    @JsonProperty("jsonFormatVersion")
    private String jsonFormatVersion;
    @JsonProperty("lastTimestamp")
    private String lastTimestamp;
    @JsonProperty("lastSnapshotID")
    private int lastSnapshotID;
    @JsonProperty("uuid")
    private String uuid;

    /**
     * Please do not remove the standard constructor (required for Jackson)
     */
    public LimineHistory() {
        this.properties = new HashMap<>();
        this.snapshotEntries = new ArrayList<>();
    }

    public String getJsonFormatVersion() {
        return jsonFormatVersion;
    }

    public void setJsonFormatVersion(String jsonFormatVersion) {
        this.jsonFormatVersion = jsonFormatVersion;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(String lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public int getLastSnapshotID() {
        return lastSnapshotID;
    }

    public void setLastSnapshotID(int lastSnapshotID) {
        this.lastSnapshotID = lastSnapshotID;
    }

    public HashMap<String, String> getProperties() {
        return properties;
    }

    public List<SnapshotEntry> getSnapshotEntries() {
        return snapshotEntries;
    }


}

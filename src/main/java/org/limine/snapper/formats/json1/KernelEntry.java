package org.limine.snapper.formats.json1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record KernelEntry(@JsonProperty("kernelVersion") String kernelVersion,
                          @JsonProperty("leadingSpaces") String leadingSpaces,
                          @JsonProperty("orderNumber") Integer orderNumber,
                          @JsonProperty("optionDetails") List<OptionDetail> optionDetails,
                          @JsonProperty("imageDetails") List<ImageDetail> imageDetails,
                          @JsonProperty("cmdlineDetails") List<CmdlineDetail> cmdlineDetails,
                          @JsonProperty("allInConfig") List<String> allInConfig,
                          @JsonProperty("allInSnapshotConfig") List<String> allInSnapshotConfig,
                          @JsonProperty("properties") Map<String, String> properties) {

    /**
     * Custom constructor with default value for properties
     */
    public KernelEntry {
        if (properties == null) {
            properties = new HashMap<>();
        }
        if (optionDetails == null) {
            optionDetails = new ArrayList<>();
        }
        if (imageDetails == null) {
            imageDetails = new ArrayList<>();
        }
        if (cmdlineDetails == null) {
            cmdlineDetails = new ArrayList<>();
        }
        if (allInConfig == null) {
            allInConfig = new ArrayList<>();
        }
        if (allInSnapshotConfig == null) {
            allInSnapshotConfig = new ArrayList<>();
        }
    }
}

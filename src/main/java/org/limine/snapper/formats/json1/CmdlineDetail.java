package org.limine.snapper.formats.json1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public record CmdlineDetail(@JsonProperty("limineKey") String limineKey,
                            @JsonProperty("leadingSpaces") String leadingSpaces,
                            @JsonProperty("lineOrderNumber") Integer lineOrderNumber,
                            @JsonProperty("cmdline") String cmdline,
                            @JsonProperty("snapshotCmdline") String snapshotCmdline,
                            @JsonProperty("parameters") Map<String, String> parameters,
                            @JsonProperty("snapshotParameters") Map<String, String> snapshotParameters,
                            @JsonProperty("properties") Map<String, String> properties) {

    /**
     * Custom constructor with default value for properties
     */
    public CmdlineDetail {
        if (parameters == null) {
            parameters = new HashMap<>();
        }
        if (snapshotParameters == null) {
            snapshotParameters = new HashMap<>();
        }
        if (properties == null) {
            properties = new HashMap<>();
        }
    }
}

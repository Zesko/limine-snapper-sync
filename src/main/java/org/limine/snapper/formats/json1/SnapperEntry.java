package org.limine.snapper.formats.json1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public record SnapperEntry(@JsonProperty("snapshotID") Integer snapshotID,
                           @JsonProperty("timestamp") String timestamp,
                           @JsonProperty("properties") Map<String, String> properties) {

    /**
     * Custom constructor with default value for properties
     */
    public SnapperEntry {
        if (properties == null) {
            properties = new HashMap<>();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SnapperEntry that = (SnapperEntry) o;
        return Objects.equals(snapshotID, that.snapshotID) && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(snapshotID, timestamp);
    }

    public String timeId() {
        return snapshotID + timestamp;
    }
}

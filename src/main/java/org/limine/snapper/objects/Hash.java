package org.limine.snapper.objects;

import java.util.Arrays;

public enum Hash {
    SHA1("sha1", "sha1sum", "_sha1_"),
    SHA256("sha256", "sha256sum", "_sha256_"),
    BLAKE3("blake3", "b3sum", "_b3_");

    public final String name;
    public final String command;
    public final String hashName;

    Hash(String name, String command, String hashName) {
        this.name = name;
        this.command = command;
        this.hashName = hashName;
    }

    public static Hash getHash(String fileName) {
        return Arrays.stream(Hash.values())
                .filter(hash -> fileName.contains(hash.hashName))
                .findFirst()
                .orElse(null);
    }

    public static Hash getHashFunction(String name) {
        return Arrays.stream(Hash.values())
                .filter(hash -> hash.name.equals(name.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

}

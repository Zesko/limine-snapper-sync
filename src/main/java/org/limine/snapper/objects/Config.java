package org.limine.snapper.objects;

import java.util.Set;

public record Config(String machineID, int maxSnapshotEntries, String espPath, String rootSubvolumePath,
                     String rootSnapshotsPath) {

    public static final String TOOL_NAME = "Limine-Snapper-Sync";
    public static final String JSON_FORMAT_VERSION = "1.0.0";
    public static final Set<Integer> SUPPORTED_LIMINE_MAJOR_VERSIONS = Set.of(8, 9);
    public static final String HISTORY_FOLDER = "limine_history";
    public static final String HISTORY_FILE = "snapshots.json";
    public static final String DEFAULT_CONFIG_FILE_PATH = "/etc/default/limine";
    public static final String[] CONFIG_FILE_PATHS = {"/etc/limine-snapper-sync.conf", DEFAULT_CONFIG_FILE_PATH, "/tmp/limine"};
    public static final String LIMINE_CONFIG_FILE = "limine.conf";
    public static final String SNAPSHOTS_COMMENT = "Selecting any snapshot to boot into it.";
    public static final String DEFAULT_TIMESTAMP = "1970-01-01 00:00:00";
    public static final String NO_FILE_NAME = "is_not_available";
    public static final String RSYNC_CMDLINE = "rsync -aAhHxX --delete --verbose ";
    public static final String SNAPPER_COMMAND = "snapper --no-headers --machine-readable csv";
    public static final String PROC_CMDLINE = "/proc/cmdline";
    public static final String MACHINE_ID = "Machine-ID=";
    public static final String BACKUP_SUFFIX = ".old";
    public static final String LOCK_RESTORE_FILE = "/tmp/limine-snapper-restore.lock";
    public static String COMMANDS_BEFORE_SAVE;
    public static String COMMANDS_AFTER_SAVE;
    public static Double LIMIT_USAGE_PERCENT = 80.0;
    public static boolean ENABLE_RSYNC_ASK = false;
    public static boolean IS_TIME_FOR_RESTORE = false; // If true, it will disable some error messages.
    public static String SNAPPER_CONFIG_NAME = null;
    public static String SNAPPER_LIST_CMDLINE = null;
    public static String TARGET_OS_NAME = null;
    public static String UUID = null;
    public static String SPACES = "";
    public static String SNAPSHOT_FORMAT_CHOICE = "0";
    public static Hash HASH_FUNCTION = Hash.SHA256;
    public static Integer BACKUP_THRESHOLD = 8;
}

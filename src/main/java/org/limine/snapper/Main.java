package org.limine.snapper;

import org.limine.snapper.formats.json1.LimineHistory;
import org.limine.snapper.formats.json1.SnapshotEntry;
import org.limine.snapper.objects.Config;
import org.limine.snapper.objects.ConsoleColor;
import org.limine.snapper.objects.LogLevel;
import org.limine.snapper.processes.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        if (args.length == 0) {
            System.err.println(ConsoleColor.RED + "No option provided." + ConsoleColor.RESET);
            Utility.loggerSend("No option provided.", LogLevel.WARNING);
            return;
        }

        for (String arg : args) {
            switch (arg) {
                case "--update" -> update();
                case "--restore" -> restore();
                case "--info" -> info();
                case "--list" -> list();
                default -> {
                    System.err.println(ConsoleColor.RED + "The option " + arg + " is unknown." + ConsoleColor.RESET);
                    return;
                }
            }
        }
    }

    private static void info() {
        // Load keywords and values from multiple config files
        Config config = new ConfigReader().readConfig();

        // Read /<esp>/<machine-id>/limine_history/snapshots.json
        String historyFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, Config.HISTORY_FILE);
        LimineHistory limineHistory = HistoryReader.load(historyFilePath);
        HistoryReader.displayInfo(limineHistory, config);
    }


    private static void list() {
        // Load keywords and values from multiple config files
        Config config = new ConfigReader().readConfig();

        // Read /<esp>/<machine-id>/limine_history/snapshots.json
        String historyFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, Config.HISTORY_FILE);
        LimineHistory limineHistory = HistoryReader.load(historyFilePath);
        HistoryReader.displaySnapshotList(limineHistory);
    }

    private static void update() throws IOException {

        // If you boot into any snapshot, do not update anything!
        if (Utility.areYouInSnapshot()) {
            String errMessage = "You are in the snapshot, the tool does not update anything!";
            Utility.errorMessage(errMessage);
            return;
        }

        if (!Utility.areYouInBtrfs()) {
            String errMessage = "You are not in Btrfs, the tool does not update anything!";
            Utility.errorMessage(errMessage);
            return;
        }

        // Load keywords and values from multiple config files
        ConfigReader configReader = new ConfigReader();
        Config config = configReader.readConfig();
        configReader.checkMountPaths(config);
        // Run SnapperReader to read a Snapper list
        configReader.readSnapperConfig();
        SnapperReader snapper = new SnapperReader();

        // Read /<esp>/<machine-id>/limine_history/snapshots.json
        String historyFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, Config.HISTORY_FILE);
        LimineHistory limineSnapshots = HistoryReader.load(historyFilePath);

        // Sync the Limine snapshots with the Snapper list
        LimineController limineController = new LimineController(config);
        limineController.sync(limineSnapshots, snapper);

        boolean needToUpdateLimineConfig = limineController.checkUpdate();

        // Run LimineReader to read the Limine config and convert all entries to a new tree node.
        LimineReader limineReader = new LimineReader(config);

        if (needToUpdateLimineConfig) {

            // Check if your target OS name is found in limine.conf
            if (limineReader.isTargetOsNotFound()) {
                String errMessage = "No target OS name in the config " + Config.DEFAULT_CONFIG_FILE_PATH;
                if (Config.TARGET_OS_NAME != null) {
                    errMessage = "OS name: '" + Config.TARGET_OS_NAME + " is not found or is wrong in " + Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE);
                }
                Utility.errorMessage(errMessage);
                return;
            }

            // Check if your OS entry has at least one kernel node.
            if (limineReader.getTargetOsNode().nodes.isEmpty() || (limineReader.getTargetOsNode().nodes.size() < 2 && limineReader.isSnapshotsNodeInsideOS())) {
                String errMessage = "Your OS entry has no kernel in " + Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE) + ". Please add at least a kernel name '//<kernel name>' and its boot config!";
                Utility.errorMessage(errMessage);
                return;
            }

            if (limineController.isAllowedToAddSnapshot()) {
                SnapshotEntry newSnapshotEntry = limineReader.createCurrentLimineEntry(snapper.getLastSnapperEntry());
                // Check the size of your ESP if the usage limit is exceeded.
                if (limineController.predictSpaceUsage(newSnapshotEntry)) {
                    // Limine history adds the new snapshot.
                    limineController.historyAddSnapshot(limineSnapshots, newSnapshotEntry);

                } else {
                    String errMessage = "Stop creating a snapshot entry because the ESP usage limit " + Config.LIMIT_USAGE_PERCENT + "% is exceeded.";
                    Utility.errorMessage(errMessage);
                }
            }

            // Overwrite /<esp>/<machine-id>/limine_history/snapshots.json
            HistoryWriter.backup(historyFilePath);
            HistoryWriter.save(limineSnapshots, historyFilePath);

        } else if (Config.UUID != null && !Config.UUID.equals(limineSnapshots.getUuid())) {
            // UUID is changed based on the user-defined configuration.
            limineSnapshots.setUuid(Config.UUID);
            HistoryWriter.save(limineSnapshots, historyFilePath);
        }

        // Save...
        LimineWriter limineWriter = new LimineWriter(config, limineReader);
        // Backup /<esp>/limine.conf
        limineWriter.backup();
        // Overwrite new snapshots and the current config in /<esp>/limine.conf
        limineWriter.setSnapshots(limineSnapshots);
        limineWriter.save();
    }

    private static void restore() throws IOException {
        // Load keywords and values from multiple config files
        Config config = new ConfigReader().readConfig();

        // Read /<esp>/<machine-id>/limine_history/snapshots.json
        String historyFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, Config.HISTORY_FILE);
        LimineHistory limineSnapshots = HistoryReader.load(historyFilePath);

        // Check if your target OS name is found in limine.conf
        LimineReader limineReader = new LimineReader(config, true);
        if (limineReader.isTargetOsNotFound()) {
            String errMessage = "No target OS name in the config " + Config.DEFAULT_CONFIG_FILE_PATH;
            if (Config.TARGET_OS_NAME != null) {
                errMessage = "OS name: '" + Config.TARGET_OS_NAME + " is not found or is wrong in " + Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE);
            }
            Utility.errorMessage(errMessage);
            return;
        }

        Config.IS_TIME_FOR_RESTORE = true;

        // Select snapshot ID for the restore.
        LimineController limineController = new LimineController(config);
        // It's time to start restoring a snapshot.
        // Copy the selected snapshot entry from snapshots.json and write it to the target OS node.
        SnapshotEntry restoredEntry = limineController.restore(limineSnapshots, limineReader);

        if (restoredEntry == null) {
            Utility.exitWithError("The restore failed.", true);
        }

        if (limineController.isBackupNeeded()) {
            HistoryWriter.backup(historyFilePath);
            // Overwrite /<esp>/<machine-id>/limine_history/snapshots.json
            HistoryWriter.save(limineSnapshots, historyFilePath);
        }

        LimineWriter limineWriter = new LimineWriter(config, limineReader);
        // Backup /<esp>/limine.conf
        limineWriter.backup();
        // Overwrite the updated snapshots-list and the current configs in /<esp>/limine.conf
        limineWriter.setTargetEntry(restoredEntry);
        limineWriter.setSnapshots(limineSnapshots);
        limineWriter.save();

        if (Utility.isCommandPresent("reboot")) {
            boolean needRestart = Utility.askUser("""
                    \s
                     The restore is complete. Please reboot now:
                     Type "[y]es" to reboot, "[n]o" or "[c]ancel" to abort rebooting.
                    \s""");
            if (needRestart) {
                Utility.runCommand("reboot", true, false);
            }
        } else {
            Utility.askUser("""
                    \s
                     The restore is complete. Please reboot now.
                     Type "[y]es" to close.
                    \s""");
        }
    }

}






package org.limine.snapper.processes;

import org.limine.snapper.formats.json1.*;
import org.limine.snapper.formats.snapper.SnapperProperty;
import org.limine.snapper.objects.Config;
import org.limine.snapper.objects.ConsoleColor;
import org.limine.snapper.objects.Output;

import java.io.File;
import java.util.*;

public class LimineController {

    private final Config config;
    private boolean isUpdateNeeded;
    private boolean isAddingSnapshotNeeded;
    private boolean isBackupNeeded;

    public LimineController(Config config) {
        this.config = config;
        isUpdateNeeded = false;
        isAddingSnapshotNeeded = false;
        isBackupNeeded = true;
    }


    /**
     * Check whether the last Snapper entry is the newest and is not yet in the Limine entries.
     * If so, add it.
     * Clean outdated Limine snapshot entries if they are not present in the Snapper list.
     */
    public void sync(LimineHistory limineHistory, SnapperReader snapper) {

        String historyTimestamp = limineHistory.getLastTimestamp();
        if (Utility.isTimeFormatNotValid(historyTimestamp)) {
            String errMessage = "Stop the tool due to a mismatch in the time format: " + historyTimestamp;
            Utility.exitWithError(errMessage);
            return;
        }

        // Delete outdated snapshot-entries from the Limine history if they have already been deleted from the Snapper list.
        // Update snapshot entries if their description has been changed in the Snapper list.
        this.update(limineHistory.getSnapshotEntries(), snapper.getSnapperEntries());

        SnapperEntry lastSnapperEntry = snapper.getLastSnapperEntry();
        if (snapper.getSnapperEntries().isEmpty() || lastSnapperEntry == null) {
            isAddingSnapshotNeeded = false;
            return;
        }

        int result = Utility.compareTimestamps(historyTimestamp, lastSnapperEntry.timestamp());
        if (result < 0) {
            // historyTimestamp is before lastSnapperTimestamp
            isAddingSnapshotNeeded = true;
        } else if (result > 0) {
            // historyTimestamp is after lastSnapperTimestamp
            isAddingSnapshotNeeded = false;
        } else {
            // historyTimestamp is equal to lastSnapperTimestamp
            isAddingSnapshotNeeded = limineHistory.getLastSnapshotID() != lastSnapperEntry.snapshotID();
        }

        if (isAddingSnapshotNeeded) {
            // Delete the oldest snapshot entry now, then a new snapshot-entry will be added later.
            this.deleteSnapshots(limineHistory.getSnapshotEntries(), config.maxSnapshotEntries() - 1);
        } else {
            // Deletion of snapshot entries according to the limit number specified in the configuration.
            this.deleteSnapshots(limineHistory.getSnapshotEntries(), config.maxSnapshotEntries());
        }
    }

    private void deleteSnapshots(List<SnapshotEntry> limineSnapshots, int limit) {
        int index = limineSnapshots.size() - limit;
        HashSet<ImageDetail> outdatedImageFiles = new HashSet<>();
        boolean shouldBeDeleted = index > 0;
        while (index > 0) {
            SnapshotEntry limineSnapshot = limineSnapshots.get(0);
            for (KernelEntry kernelEntry : limineSnapshot.kernelEntries()) {
                outdatedImageFiles.addAll(kernelEntry.imageDetails());
            }
            limineSnapshots.remove(0);
            this.isUpdateNeeded = true;
            index--;
        }
        if (shouldBeDeleted) {
            this.deleteImageFiles(limineSnapshots, outdatedImageFiles);
        }
    }

    private void update(List<SnapshotEntry> limineSnapshots, HashMap<String, SnapperEntry> snapperList) {
        Iterator<SnapshotEntry> iterator = limineSnapshots.iterator();
        HashSet<ImageDetail> outdatedImageFiles = new HashSet<>();
        boolean shouldFilesBeDeleted = false;
        while (iterator.hasNext()) {
            SnapshotEntry limineSnapshot = iterator.next();
            SnapperEntry snapperEntry = snapperList.get(limineSnapshot.snapperEntry().timeId());
            if (snapperEntry == null) {
                for (KernelEntry kernelEntry : limineSnapshot.kernelEntries()) {
                    outdatedImageFiles.addAll(kernelEntry.imageDetails());
                }
                iterator.remove();
                this.isUpdateNeeded = true;
                shouldFilesBeDeleted = true;
            } else {
                // Update the Limine snapshot if the description of the Snapper entry has been changed.
                String oldDescription = limineSnapshot.snapperEntry().properties().get(SnapperProperty.DESCRIPTION.replace);
                String newDescription = snapperEntry.properties().get(SnapperProperty.DESCRIPTION.replace);
                if (!Objects.equals(oldDescription, newDescription)) {
                    limineSnapshot.snapperEntry().properties().put(SnapperProperty.DESCRIPTION.replace, newDescription);
                    this.isUpdateNeeded = true;
                }
            }
        }
        if (shouldFilesBeDeleted) {
            this.deleteImageFiles(limineSnapshots, outdatedImageFiles);
        }
    }

    private void deleteImageFiles(List<SnapshotEntry> limineSnapshots, HashSet<ImageDetail> outdatedImageFiles) {
        // Collect all files from all current snapshots after you have deleted some snapshots.
        HashSet<String> allImageFiles = this.getCurrentImageFiles(limineSnapshots);
        // Deletion of outdated files if they are not used by all snapshots.
        for (ImageDetail imageDetail : outdatedImageFiles) {
            if (!allImageFiles.contains(imageDetail.fileHashName())) {
                // Run a command to delete /<esp>/<machine-id>/limine_history/<some file>
                String targetHashFile = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                Utility.deleteFile(targetHashFile);
            }
        }
    }

    private HashSet<String> getCurrentImageFiles(List<SnapshotEntry> limineSnapshots) {
        HashSet<String> allImageFiles = new HashSet<>();
        for (SnapshotEntry snapshotEntry : limineSnapshots) {
            for (KernelEntry kernelEntry : snapshotEntry.kernelEntries()) {
                for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                    String fileHashName = imageDetail.fileHashName();
                    allImageFiles.add(fileHashName);
                }
            }
        }
        return allImageFiles;
    }

    /**
     * Copy files to the history folder with deduplication function.
     * The Limine history adds the last snapshot.
     */
    public void historyAddSnapshot(LimineHistory limineHistory, SnapshotEntry lastSnapshotEntry) {
        if (lastSnapshotEntry.kernelEntries().isEmpty()) {
            if (!Config.IS_TIME_FOR_RESTORE) {
                // The current snapshot entry has no kernel entry, it should not be added to the Limine menu.
                String errMessage = "The Limine snapshot entry cannot be created because it has no kernel.";
                Utility.errorMessage(errMessage);
            }
            return;
        }

        // Copy or move any image files to the Limine history directory.
        for (KernelEntry kernelEntry : lastSnapshotEntry.kernelEntries()) {
            for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                if (Config.IS_TIME_FOR_RESTORE && imageDetail.fileHashName().endsWith(Config.NO_FILE_NAME)) {
                    // There is no image file. No need to back up this file during the restore.
                    continue;
                }
                String sourceFilePath = Utility.buildPath(config.espPath(), imageDetail.espDirPath(), imageDetail.fileName());
                String targetFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                if (Utility.isFilePresent(targetFilePath)) {
                    this.repairIfCorrupted(sourceFilePath, targetFilePath);
                    if (Config.IS_TIME_FOR_RESTORE && Utility.isFilePresent(sourceFilePath)) {
                        // Delete the image file as it already exists in the Limine history directory
                        Utility.deleteFile(sourceFilePath);
                    }
                    continue;
                }
                isUpdateNeeded = true;
                if (Config.IS_TIME_FOR_RESTORE) {
                    // Move the image file to the Limine history directory
                    Utility.moveFile(sourceFilePath, targetFilePath, true);
                } else {
                    // Copy the image file to the Limine history directory
                    Utility.copyFile(sourceFilePath, targetFilePath, true);
                }
            }
        }
        limineHistory.getSnapshotEntries().add(lastSnapshotEntry);
        this.updateHistoryDetail(limineHistory, lastSnapshotEntry.snapperEntry());
    }

    public void updateHistoryDetail(LimineHistory limineHistory, SnapperEntry lastSnapperEntry) {
        limineHistory.setLastTimestamp(lastSnapperEntry.timestamp());
        limineHistory.setLastSnapshotID(lastSnapperEntry.snapshotID());
    }

    /**
     * User is prompted to confirm the snapshot ID or select a different snapshot ID for restoring.
     */
    public int checkAndSelectSnapshotID(LimineHistory limineHistory) {
        int snapshotID = Utility.validateAndGetSnapshotID();
        SnapshotEntry snapshotEntry = null;
        // Check the snapshot ID if it is present in the Limine history.
        for (SnapshotEntry entry : limineHistory.getSnapshotEntries()) {
            if (entry.snapperEntry().snapshotID() == snapshotID) {
                snapshotEntry = entry;
                break;
            }
        }
        if (snapshotEntry == null) {
            snapshotID = -1;
        } else {
            System.out.println(ConsoleColor.GREEN_BOLD + "Snapshot ID : " + ConsoleColor.RESET + snapshotID);
            System.out.println(ConsoleColor.GREEN_BOLD + "Date        : " + ConsoleColor.RESET + snapshotEntry.snapperEntry().timestamp());
            System.out.println(ConsoleColor.GREEN_BOLD + "Description : " + ConsoleColor.RESET + snapshotEntry.snapperEntry().properties().get(SnapperProperty.DESCRIPTION.replace));
        }

        if (snapshotID > 0) {
            // Ask user to confirm to restore this snapshot?
            boolean isConfirmed = Utility.askUser("\n Confirm that you want to restore snapshot ID: " + snapshotID + "?\n"
                    + " If no, all Limine snapshots will be displayed for selection.\n"
                    + " Type \"[y]es\" to confirm, \"[n]o\" to list available snapshots, or \"[c]ancel\" to abort the restore.\n");

            if (!isConfirmed) {
                snapshotID = this.selectSnapshot(limineHistory);
            }
        } else {
            // Ask user to select which snapshot ID for restore.
            snapshotID = this.selectSnapshot(limineHistory);
        }
        return snapshotID;
    }

    /**
     * Restoration process
     */
    public SnapshotEntry restore(LimineHistory snapshots, LimineReader limineReader) {
        boolean areYouInSnapshot = Utility.areYouInSnapshot();
        int snapshotID = this.checkAndSelectSnapshotID(snapshots);
        SnapshotEntry limineEntry = limineReader.createCurrentLimineEntry();

        SnapshotEntry snapshotEntry = null;
        for (SnapshotEntry entry : snapshots.getSnapshotEntries()) {
            if (entry.snapperEntry().snapshotID() == snapshotID) {
                snapshotEntry = entry;
                break;
            }
        }

        if (snapshotEntry == null) {
            String errMessage = "No snapshot " + snapshotID + " is found";
            Utility.errorMessage(errMessage);
            return null;
        }

        // Let people decide what recovery method in the terminal.
        boolean isRsyncInstalled = Utility.isCommandPresent("rsync");
        Config.ENABLE_RSYNC_ASK = Config.ENABLE_RSYNC_ASK && isRsyncInstalled;

        if (Config.ENABLE_RSYNC_ASK) {
            boolean isRsyncEnabled = Utility.askUser("""
                    \s
                     Do you want to use "rsync" for restore?
                     If not, "btrfs" restore will be used.
                     Type "[y]es", "[n]o", or "[c]ancel" to abort the restore.
                    \s""");

            if (isRsyncEnabled) {
                this.isBackupNeeded = Utility.askUser("""
                        \s
                         Do you want to create a new "backup" snapshot before starting rsync?
                         Type "[y]es", "[n]o", or "[c]ancel" to abort the restore.
                        \s""");

            } else {
                Config.ENABLE_RSYNC_ASK = false;
            }
        }

        // Predict if there is enough partition space when adding new files?
        if (!predictSpaceUsage(limineEntry, snapshotEntry)) {
            String errMessage = "Stopping restore to prevent the ESP from running out of space, which could break restored boot files.";
            Utility.errorMessage(errMessage);
            return null;
        }

        String mountDir = "/run/lss/";
        String timestamp = Utility.getCurrentIsoFormattedTime();
        String subvolumePath = Utility.removeSurroundingSlashes(config.rootSubvolumePath());
        String backupSubvolume = subvolumePath + "_backup_" + timestamp;
        String newSubvolume = subvolumePath + "_lss_" + timestamp;
        String snapshotPath = Utility.removeSurroundingSlashes(config.rootSnapshotsPath());
        String backupDescription = "Backup";
        String uuid;

        if (Config.UUID != null) {
            uuid = Config.UUID;
        } else {
            uuid = snapshots.getUuid();
        }

        if (this.isBackupNeeded) {
            backupDescription = Utility.userInput("\n Enter a description for the \"backup\" of the subvolume: " + subvolumePath);
            if (backupDescription.isEmpty()) {
                backupDescription = "Backup";
            }
        }

        // mount /run/lss
        List<String> commands = List.of(
                "mount -t btrfs -o subvol=/ /dev/disk/by-uuid/" + uuid + " " + mountDir
        );

        // Time to start mount.
        Utility.infoMessage("\nTime to start mounting Btrfs.");
        if (!Utility.ensureDirectoryExists(mountDir)) {
            return null;
        }
        if (!Utility.runCommands(commands, true, true, true)) {
            String errMessage = "Failed to mount " + mountDir;
            Utility.errorMessage(errMessage);
            Utility.runCommand("umount " + mountDir, false, false);
            return null;
        }

        // Find all child subvolumes of the parent subvolume. This is important because rsync should exclude them.
        // For Btrfs restore, they should be moved back to the parent subvolume after the restore.
        List<String> childSubvolumePaths = new ArrayList<>();
        // Get all child subvolume-paths
        String btrfsCommand = "btrfs subvolume list -o '" + Utility.buildPath(mountDir, subvolumePath) + "'";
        Output subvolumePathsOutput = Utility.getTextFromCommand(btrfsCommand, true, null);
        if (subvolumePathsOutput.isSuccess()) {
            for (String path : subvolumePathsOutput.text()) {
                String lastWord = Utility.extractLastWord(path);
                if (lastWord.startsWith(subvolumePath + File.separator)) {
                    // Remove subvolume name from the child subvolume path
                    String childSubvolumePath = lastWord.substring(subvolumePath.length() + 1);
                    childSubvolumePaths.add(childSubvolumePath);
                } else {
                    String errMessage = "The output format of 'btrfs subvolume list -o '" + Utility.buildPath(mountDir, subvolumePath) + "' was changed. Please report the issue.";
                    Utility.errorMessage(errMessage);
                    return null;
                }
            }
        }

        boolean isSnapshot = Utility.isDirectoryPresent(Utility.buildPath(mountDir, snapshotPath, snapshotID + "/snapshot/"));

        if (isSnapshot && Config.ENABLE_RSYNC_ASK) {
            // Rsync instead Btrfs restore
            StringBuilder excludes = new StringBuilder();
            for (String childSubvolumePath : childSubvolumePaths) {
                excludes.append(" --exclude ").append("'").append(childSubvolumePath).append("'");
            }
            String snapshotDirPath = Utility.buildPath(mountDir, snapshotPath, snapshotID + "/snapshot") + File.separator;
            String subvolumeDirPath = Utility.buildPath(mountDir, subvolumePath) + File.separator;
            String rsyncCmdline = Config.RSYNC_CMDLINE + excludes + " '" + snapshotDirPath + "' '" + subvolumeDirPath + "'";

            if (this.isBackupNeeded) {
                commands = List.of(
                        // Bugfix: Do not use the "-r" read-only option as it denies moving the snapshot to the Snapper list.
                        "btrfs subvolume snapshot '" + Utility.buildPath(mountDir, subvolumePath) + "' '" + Utility.buildPath(mountDir, backupSubvolume) + "'",
                        rsyncCmdline
                );
            } else {
                commands = List.of(rsyncCmdline);
            }
        } else if (isSnapshot) {
            // Restore the snapshot in any (default or custom) Snapper layout.
            String snapshotDirPath = Utility.buildPath(mountDir, snapshotPath, snapshotID + "/snapshot");
            String newSubvolumeDirPath = Utility.buildPath(mountDir, newSubvolume);
            String subvolumeDirPath = Utility.buildPath(mountDir, subvolumePath);
            String backupSubvolumeDirPath = Utility.buildPath(mountDir, backupSubvolume);

            commands = List.of(
                    "btrfs subvolume snapshot '" + snapshotDirPath + "' '" + newSubvolumeDirPath + "'",
                    "mv '" + subvolumeDirPath + "' '" + backupSubvolumeDirPath + "'",
                    "mv '" + newSubvolumeDirPath + "' '" + subvolumeDirPath + "'"
            );
        } else {
            String errMessage = "It does not support your Btrfs layout without Snapper.";
            Utility.errorMessage(errMessage);
            return null;
        }

        // Time to start to restore.
        Utility.infoMessage("Time to start restoring snapshot ID: " + snapshotID);
        if (!Utility.runCommands(commands, true, true, true)) {
            if (!Config.ENABLE_RSYNC_ASK) {
                // The restore fails, then "backup" subvolume should be moved back to the correct location.
                if (Utility.isDirectoryPresent(Utility.buildPath(mountDir, backupSubvolume))) {
                    List<String> revert;
                    if (Utility.isDirectoryPresent(Utility.buildPath(mountDir, subvolumePath))) {

                        String subvolumeDirPath = Utility.buildPath(mountDir, subvolumePath);
                        String newSubvolumeDirPath = Utility.buildPath(mountDir, newSubvolume);
                        String backupSubvolumeDirPath = Utility.buildPath(mountDir, backupSubvolume);

                        revert = List.of(
                                "mv '" + subvolumeDirPath + "' '" + newSubvolumeDirPath + "'",
                                "mv '" + backupSubvolumeDirPath + "' '" + subvolumeDirPath + "'"
                        );
                    } else {
                        revert = List.of(
                                "mv '" + Utility.buildPath(mountDir, backupSubvolume) + "' '" + Utility.buildPath(mountDir, subvolumePath) + "'"
                        );
                    }
                    Utility.runCommands(revert, true, true, true);
                }
            }
            String errMessage = "Failed to restore.";
            Utility.errorMessage(errMessage);
            return null;
        }

        if (!Config.ENABLE_RSYNC_ASK) {
            // If the parent backup-subvolume has multiple child subvolumes,
            // they should be moved back to the right subvolume.
            List<String> movedChildSubvolumeCommands = new ArrayList<>();
            for (String childSubvolumePath : childSubvolumePaths) {
                String sourceDirPath = Utility.buildPath(mountDir, backupSubvolume, childSubvolumePath);
                String extractDirectoryPath = Utility.extractDirectoryPath(childSubvolumePath);
                String targetDirPath = Utility.buildPath(mountDir, subvolumePath, extractDirectoryPath) + File.separator;
                movedChildSubvolumeCommands.add("mv '" + sourceDirPath + "' '" + targetDirPath + "'");
            }

            if (!movedChildSubvolumeCommands.isEmpty()) {
                // Time to move all child subvolumes back.
                Utility.infoMessage("Time to move all child subvolumes back to the parent subvolume: " + subvolumePath);
                if (!Utility.runCommands(movedChildSubvolumeCommands, false, true, true)) {
                    String errMessage = "Failed to moved the child subvolumes.";
                    Utility.errorMessage(errMessage);
                }
            }
        }

        // Move the backup subvolume to the Snapper list, move its kernel files to the Limine history, and then update snapshots.json.
        if (this.isBackupNeeded) {
            Utility.infoMessage("Time to add the \"" + backupDescription + "\" entry to the Snapper list and Limine snapshot list.");
            int latestSnapshotID = Utility.findLatestSnapperNumber(Utility.buildPath(mountDir, snapshotPath));
            if (latestSnapshotID < 1) {
                String errMessage = "Oops, but after the restore there is no snapshot in the Snapper list. That is strange.";
                Utility.errorMessage(errMessage);
            } else {
                if (areYouInSnapshot) {
                    // You are in the snapshot without the conflict with the Snapper.
                    latestSnapshotID = latestSnapshotID + 1;
                } else {
                    // You are not in the snapshot.
                    // If the tool and the Snapper create the same ID at the same time, this leads to a conflict.
                    // Solution: Another higher ID is created, probably to avoid the conflict with the Snapper.
                    latestSnapshotID = latestSnapshotID + 2;
                }

                String backupSubvolumeDirPath = Utility.buildPath(mountDir, backupSubvolume);
                String newSnapshotDirPath = Utility.buildPath(mountDir, snapshotPath, String.valueOf(latestSnapshotID));
                Utility.ensureDirectoryExists(newSnapshotDirPath);

                List<String> moveSubvolumeToSnapper = List.of(
                        "mv '" + backupSubvolumeDirPath + "' '" + newSnapshotDirPath + "/snapshot'",
                        "btrfs property set '" + newSnapshotDirPath + "/snapshot' ro true"
                );
                if (Utility.runCommands(moveSubvolumeToSnapper, false, true, true)) {
                    // Create a new info.xml for this snapshot in the Snapper list.
                    String utcTime = Utility.getCurrentUTCTime();
                    SnapperWriter.writeInfoXml(String.valueOf(latestSnapshotID), utcTime, backupDescription, newSnapshotDirPath + "/info.xml");

                    // Create a new backup (snapshot) entry with the "broken" kernel versions
                    String localTime = Utility.convertUTCToLocal(utcTime);
                    SnapperEntry snapperEntry = new SnapperEntry(latestSnapshotID, localTime, null);
                    snapperEntry.properties().put(SnapperProperty.DESCRIPTION.replace, backupDescription);
                    SnapshotEntry backupEntry = limineReader.createCurrentLimineEntry(snapperEntry);

                    // Move the "broken" kernel files to the Limine history directory.
                    // snapshots.json (the Limine history) adds the backup entry.
                    this.historyAddSnapshot(snapshots, backupEntry);

                } else {
                    String errMessage = "Failed to move the \"backup\" snapshot to the Snapper list.";
                    Utility.errorMessage(errMessage);
                }
            }
        } else {
            // Delete the "broken" kernel files without a backup.
            for (KernelEntry kernelEntry : limineEntry.kernelEntries()) {
                for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                    if (!imageDetail.fileHashName().endsWith(Config.NO_FILE_NAME)) {
                        String sourceFilePath = Utility.buildPath(config.espPath(), imageDetail.espDirPath(), imageDetail.fileName());
                        if (Utility.isFilePresent(sourceFilePath)) {
                            String targetFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                            this.repairIfCorrupted(sourceFilePath, targetFilePath);
                            Utility.deleteFile(sourceFilePath);
                        }
                    }
                }
            }
        }

        // Time to copy the matching kernel-files of this snapshot to their right place.
        Utility.infoMessage("Time to restore matching kernel versions.");
        for (KernelEntry kernelEntry : snapshotEntry.kernelEntries()) {
            for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                if (!imageDetail.fileHashName().endsWith(Config.NO_FILE_NAME)) {
                    String sourceFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                    String targetDirPath = Utility.buildPath(config.espPath(), imageDetail.espDirPath()) + File.separator;
                    // Check if the directory does not exist, then create it.
                    Utility.ensureDirectoryExists(targetDirPath);
                    String targetFilePath = targetDirPath + imageDetail.fileName();

                    // Check if the file exists
                    if (Utility.isFilePresent(targetFilePath)) {
                        String hashCode = Utility.calculateHash(targetFilePath);
                        // Check if the file is the same as it from the snapshot.
                        if (!imageDetail.fileHashName().endsWith(hashCode)) {
                            // Both files are different,
                            // The file should be replaced by the other file from the snapshot.
                            Utility.copyFile(sourceFilePath, targetFilePath, true);
                        }
                    } else {
                        Utility.copyFile(sourceFilePath, targetFilePath, true);
                    }
                }
            }
        }
        // Overwrite the restored kernel entries in the current Limine entry.
        limineEntry.kernelEntries().clear();
        limineEntry.kernelEntries().addAll(snapshotEntry.kernelEntries());
        return limineEntry;
    }

    private void repairIfCorrupted(String sourceFilePath, String targetFilePath) {
        if (Utility.isFilePresent(targetFilePath)) {
            // Check if the target file in any of the snapshots is corrupted.
            String hashCode = Utility.calculateHash(targetFilePath);
            if (!targetFilePath.endsWith(hashCode)) {
                // The snapshot file is corrupted.
                // Replace the corrupted file with a healthy one.
                Utility.copyFile(sourceFilePath, targetFilePath, true);
                String errMessage = "A corrupted file was detected at: '" + targetFilePath + "' and has been replaced with a healthy one.";
                Utility.errorMessage(errMessage);
            }
        }
    }

    private int selectSnapshot(LimineHistory snapshots) {
        HashSet<String> IDs = HistoryReader.displaySnapshotList(snapshots);
        int selectedID;
        while (true) {
            String input = Utility.userSelect("""
                    \s
                     Which snapshot ID do you want to restore?
                     Type an ID or "[c]ancel" to abort the restore.
                    \s""");

            if (IDs.contains(input.trim())) {
                selectedID = Integer.parseInt(input.trim());
                break;
            } else {
                System.err.println(ConsoleColor.RED + "Selected ID is invalid or not available: " + ConsoleColor.RESET + input);
            }
        }
        return selectedID;
    }

    public boolean predictSpaceUsage(SnapshotEntry currentEntry) {
        HashSet<String> addedFilePaths = new HashSet<>();
        for (KernelEntry kernelEntry : currentEntry.kernelEntries()) {
            for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                String snapshotFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                if (!Utility.isFilePresent(snapshotFilePath)) {
                    String filePath = Utility.buildPath(config.espPath(), imageDetail.espDirPath(), imageDetail.fileName());
                    addedFilePaths.add(filePath);
                }
            }
        }
        return Utility.doesPartitionHaveSpace(Config.LIMIT_USAGE_PERCENT, config.espPath(), addedFilePaths, new HashSet<>());
    }

    private boolean predictSpaceUsage(SnapshotEntry currentEntry, SnapshotEntry snapshotEntry) {
        HashSet<String> addedFilePaths = new HashSet<>();
        HashSet<String> deletedFilePaths = new HashSet<>();
        for (KernelEntry kernelEntry : currentEntry.kernelEntries()) {
            for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                String filePath = Utility.buildPath(config.espPath(), imageDetail.espDirPath(), imageDetail.fileName());
                if (isBackupNeeded) {
                    String snapshotFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                    if (Utility.isFilePresent(filePath) && Utility.isFilePresent(snapshotFilePath)) {
                        deletedFilePaths.add(filePath);
                    }
                } else {
                    if (Utility.isFilePresent(filePath)) {
                        deletedFilePaths.add(filePath);
                    }
                }
            }
        }
        for (KernelEntry kernelEntry : snapshotEntry.kernelEntries()) {
            for (ImageDetail imageDetail : kernelEntry.imageDetails()) {
                String snapshotFilePath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER, imageDetail.fileHashName());
                if (Utility.isFilePresent(snapshotFilePath)) {
                    addedFilePaths.add(snapshotFilePath);
                }
            }
        }
        return Utility.doesPartitionHaveSpace(100.0, config.espPath(), addedFilePaths, deletedFilePaths);
    }

    public boolean checkUpdate() {
        return isUpdateNeeded || isAddingSnapshotNeeded;
    }

    public boolean isAllowedToAddSnapshot() {
        return isAddingSnapshotNeeded;
    }

    public boolean isBackupNeeded() {
        return this.isBackupNeeded;
    }
}

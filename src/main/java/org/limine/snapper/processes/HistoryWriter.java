package org.limine.snapper.processes;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.limine.snapper.formats.json1.LimineHistory;
import org.limine.snapper.objects.Config;

import java.io.File;
import java.io.IOException;

public class HistoryWriter {

    /**
     * Save Limine history.
     */
    public static void save(LimineHistory limineHistory, String filePath) throws IOException {
        updateDetail(limineHistory);
        Utility.ensurePathExists(filePath);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(filePath), limineHistory);
        Utility.infoMessage("Saved successfully:", filePath);
    }

    public static void backup(String historyFilePath) {
        if (Utility.isFilePresent(historyFilePath)) {
            // Auto time-based backup. Check if an older backup exists.
            String backupPath = historyFilePath + Config.BACKUP_SUFFIX;
            boolean isBackupNeeded = true;
            if (Utility.isFilePresent(backupPath)) {
                isBackupNeeded = Utility.isFileOlderThan(backupPath, Config.BACKUP_THRESHOLD);
            }
            if (isBackupNeeded) {
                Utility.copyFile(historyFilePath, backupPath, true, false, false);
            }
        }
    }

    private static void updateDetail(LimineHistory limineHistory) {
        if (Config.UUID != null && !Config.UUID.equals(limineHistory.getUuid())) {
            // UUID is changed based on the user-defined config.
            limineHistory.setUuid(Config.UUID);
            return;
        }
        if (Config.IS_TIME_FOR_RESTORE) {
            return;
        }
        if (limineHistory.getUuid() == null || limineHistory.getUuid().isEmpty()) {
            limineHistory.setUuid(Utility.getUuid());
        }
    }
}

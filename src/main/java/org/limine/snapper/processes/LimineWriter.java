package org.limine.snapper.processes;

import org.limine.snapper.formats.json1.KernelEntry;
import org.limine.snapper.formats.json1.LimineHistory;
import org.limine.snapper.formats.json1.SnapshotEntry;
import org.limine.snapper.formats.limine8.LimineKey;
import org.limine.snapper.formats.snapper.SnapperProperty;
import org.limine.snapper.objects.Config;
import org.limine.snapper.objects.TreeNode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class LimineWriter {

    private final Config config;
    private final LimineReader limineReader;

    public LimineWriter(Config config, LimineReader limineReader) {
        this.config = config;
        this.limineReader = limineReader;
    }

    /**
     * Back up limine.conf
     */
    public void backup() {
        String limineConfigFilePath = Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE);
        if (Utility.isFilePresent(limineConfigFilePath)) {
            // Auto time-based backup. Check if an older backup exists.
            String backupPath = limineConfigFilePath + Config.BACKUP_SUFFIX;
            boolean isBackupNeeded = true;
            if (Utility.isFilePresent(backupPath)) {
                isBackupNeeded = Utility.isFileOlderThan(backupPath, Config.BACKUP_THRESHOLD);
            }
            if (isBackupNeeded) {
                Utility.copyFile(limineConfigFilePath, backupPath, true, false, false);
            }
        }
    }

    public void setSnapshots(LimineHistory snapshots) {
        if (!limineReader.isSnapshotsNodeFound()) {
            // The OS node does not have a "snapshots" node, so a default "//Snapshots" node is created.
            List<TreeNode> kernelNodes = limineReader.getTargetOsNode().nodes;
            TreeNode snapshotsNode = new TreeNode(Config.SPACES + LimineKey.SNAPSHOTS_INSIDE_OS_NODE, 1, limineReader.getTargetOsNode().name);
            kernelNodes.add(kernelNodes.size(), snapshotsNode);
        }

        TreeNode snapshotsNode = limineReader.getTargetOsNode().findNodeByName(LimineKey.SNAPSHOTS_INSIDE_OS_NODE.toString(), 1);
        if (snapshotsNode != null) {
            // Save all snapshots (from the history) in the Snapshots-node inside your OS node.
            this.saveHistoryToSnapshotsNode(snapshotsNode, snapshots.getSnapshotEntries(), true);
            return;
        }

        snapshotsNode = limineReader.getRootNode().findNodeByName(LimineKey.SNAPSHOTS_OUTSIDE_OS_NODE.toString(), 1);
        if (snapshotsNode != null) {
            // Save all snapshots (from the history) in the Snapshots-node outside your OS node.
            this.saveHistoryToSnapshotsNode(snapshotsNode, snapshots.getSnapshotEntries(), false);
            return;
        }
        // Check if "//Snapshots" or "/Snapshots" exists in limine.conf
        String errMessage = "'" + LimineKey.SNAPSHOTS_INSIDE_OS_NODE + "' or '" + LimineKey.SNAPSHOTS_OUTSIDE_OS_NODE + "' is not found in " + Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE);
        Utility.exitWithError(errMessage);
    }

    public void setTargetEntry(SnapshotEntry targetEntry) {
        // Save the restored entry (from the Limine snapshots) in the OS node.
        this.saveEntryToTargetNode(limineReader.getTargetOsNode(), targetEntry);
    }

    /**
     * Save the tree node in limine.conf
     */
    public void save() {
        List<String> lines = new ArrayList<>();
        this.writeNodeToLines(limineReader.getRootNode(), lines, 0);
        Path limineConfigPath = Paths.get(Utility.buildPath(config.espPath(), Config.LIMINE_CONFIG_FILE));
        // Check if "/<esp>/limine.conf" exists.
        if (Files.notExists(limineConfigPath)) {
            String errMessage = "File path: " + limineConfigPath + " does not exit!";
            Utility.exitWithError(errMessage);
            return;
        }
        try {
            //Run commands before save
            if (Config.COMMANDS_BEFORE_SAVE != null && !Config.COMMANDS_BEFORE_SAVE.isEmpty()) {
                Utility.runCommand(Config.COMMANDS_BEFORE_SAVE, true, false);
            }
            Files.write(limineConfigPath, lines, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            Utility.infoMessage("Saved successfully:", limineConfigPath.toString());
            //Run command after save
            if (Config.COMMANDS_AFTER_SAVE != null && !Config.COMMANDS_AFTER_SAVE.isEmpty()) {
                Utility.runCommand(Config.COMMANDS_AFTER_SAVE, true, false);
            }
        } catch (IOException e) {
            String errMessage = "Error writing file: " + e.getMessage();
            Utility.exitWithError(errMessage);
        }
    }

    private void saveEntryToTargetNode(TreeNode targetNode, SnapshotEntry targetEntry) {
        TreeNode snapshotsNode = targetNode.findNodeByName(LimineKey.SNAPSHOTS_INSIDE_OS_NODE.toString(), 1);
        targetNode.nodes.clear();
        for (KernelEntry kernelEntry : targetEntry.kernelEntries()) {
            String kernelVersion = kernelEntry.leadingSpaces() + LimineKey.SUB_ENTRY_KEY + kernelEntry.kernelVersion();
            TreeNode kernelNode = new TreeNode(kernelVersion, 2, Config.TARGET_OS_NAME);
            kernelNode.configLines.addAll(kernelEntry.allInConfig());
            targetNode.nodes.add(kernelNode);
        }
        if (snapshotsNode != null) {
            targetNode.nodes.add(snapshotsNode);
        }
        targetNode.enableLineBreak = true;
    }

    /**
     * Overwrite the updated snapshot entries in the snapshots-node.
     */
    private void saveHistoryToSnapshotsNode(TreeNode snapshotsNode, List<SnapshotEntry> limineSnapshotEntries, boolean isSnapshotsInsideOsNode) {
        if (snapshotsNode == null) {
            return;
        }
        int maxWidth = 0;
        if (!limineSnapshotEntries.isEmpty()) {
            Integer lastSnapshotID = limineSnapshotEntries.get(limineSnapshotEntries.size() - 1).snapperEntry().snapshotID();
            maxWidth = String.valueOf(lastSnapshotID).length();
        }
        if (isSnapshotsInsideOsNode) {
            // Snapshots-node "//Snapshots" is inside your OS node
            snapshotsNode.configLines.clear();
            snapshotsNode.nodes.clear();
            snapshotsNode.addConfigLine(Config.SPACES + LimineKey.COMMENT + " " + Config.SNAPSHOTS_COMMENT);
            snapshotsNode.enableLineBreak = true;

            for (SnapshotEntry snapshotEntry : limineSnapshotEntries) {
                String snapshotName = this.snapshotNameFormat(maxWidth, snapshotEntry.snapperEntry().snapshotID(), snapshotEntry.snapperEntry().timestamp());
                TreeNode snapshotNode = new TreeNode(snapshotName, 3, LimineKey.SNAPSHOTS_INSIDE_OS_NODE.toString());
                snapshotNode.addConfigLine(Config.SPACES + LimineKey.COMMENT + " " + snapshotEntry.snapperEntry().properties().get(SnapperProperty.DESCRIPTION.replace));
                for (KernelEntry kernelEntry : snapshotEntry.kernelEntries()) {
                    if (kernelEntry.allInSnapshotConfig().isEmpty()) {
                        continue;
                    }
                    String kernelName = Config.SPACES + LimineKey.SUB_X3_ENTRY_KEY + kernelEntry.kernelVersion();
                    TreeNode kernelNode = new TreeNode(kernelName, 4, snapshotName);
                    for (String snapConfigLine : kernelEntry.allInSnapshotConfig()) {
                        kernelNode.configLines.add(Config.SPACES + snapConfigLine.trim());
                    }
                    snapshotNode.addNode(kernelNode);
                }
                if (snapshotNode.nodes.isEmpty()) {
                    continue;
                }
                // Insert the snapshots-node at the beginning of the list.
                snapshotsNode.nodes.addFirst(snapshotNode);
            }

        } else {
            // Snapshots-node "/Snapshots" is outside your OS node.
            // Search your OS as sub snapshots-node inside the snapshots-node.
            // If not available, create a new sub snapshots-node of your OS inside the snapshots-node.
            TreeNode osNode = snapshotsNode.findNodeByIDorName(config, Config.TARGET_OS_NAME, 1);
            if (osNode == null) {
                // Create a new node of your OS snapshots
                String osName = Config.SPACES + LimineKey.SUB_ENTRY_KEY + Config.TARGET_OS_NAME;
                osNode = new TreeNode(osName, 2, LimineKey.SNAPSHOTS_OUTSIDE_OS_NODE.toString());
                snapshotsNode.addNode(osNode);
            } else {
                osNode.configLines.clear();
                osNode.nodes.clear();
            }
            // Update the OS name - rename the old OS name to the new OS name
            osNode.name = Config.SPACES + LimineKey.SUB_ENTRY_KEY + Config.TARGET_OS_NAME;
            osNode.cleanName = Config.TARGET_OS_NAME;
            osNode.addConfigLine(Config.SPACES + LimineKey.COMMENT + " " + Config.TARGET_OS_NAME + " snapshots");
            osNode.addConfigLine(Config.SPACES + LimineKey.COMMENT + " " + Config.MACHINE_ID + config.machineID());
            osNode.enableLineBreak = true;

            for (SnapshotEntry snapshotEntry : limineSnapshotEntries) {
                String snapshotName = this.snapshotNameFormat(maxWidth, snapshotEntry.snapperEntry().snapshotID(), snapshotEntry.snapperEntry().timestamp());
                TreeNode snapshotNode = new TreeNode(snapshotName, 3, Config.TARGET_OS_NAME);
                snapshotNode.addConfigLine(Config.SPACES + LimineKey.COMMENT + " " + snapshotEntry.snapperEntry().properties().get(SnapperProperty.DESCRIPTION.replace));
                for (KernelEntry kernelEntry : snapshotEntry.kernelEntries()) {
                    if (kernelEntry.allInSnapshotConfig().isEmpty()) {
                        continue;
                    }
                    String kernelName = Config.SPACES + LimineKey.SUB_X3_ENTRY_KEY + kernelEntry.kernelVersion();
                    TreeNode kernelNode = new TreeNode(kernelName, 4, snapshotName);
                    for (String snapConfigLine : kernelEntry.allInSnapshotConfig()) {
                        kernelNode.configLines.add(Config.SPACES + snapConfigLine.trim());
                    }
                    snapshotNode.addNode(kernelNode);
                }
                if (snapshotNode.nodes.isEmpty()) {
                    continue;
                }
                // Insert the snapshots-node at the beginning of the list.
                osNode.nodes.addFirst(snapshotNode);
            }
        }
    }

    private String snapshotNameFormat(int maxWidth, Integer snapshotID, String timestamp) {
        // Format snapshotID to be left-aligned with dynamic width.
        // Note: Do not use "right-aligned" as Limine removes right-aligned spaces by default.
        String formattedSnapshotID = String.format("%-" + maxWidth + "d", snapshotID);
        return switch (Config.SNAPSHOT_FORMAT_CHOICE) {
            case "1" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + formattedSnapshotID + "│" + timestamp;
            case "2" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + formattedSnapshotID + " │ " + timestamp;
            case "3" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + timestamp + "│" + snapshotID;
            case "4" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + timestamp + " │ " + snapshotID;
            case "5" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + timestamp;
            case "6" -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + snapshotID;
            default -> Config.SPACES + LimineKey.SUB_X2_ENTRY_KEY + "ID=" + formattedSnapshotID + " " + timestamp;
        };
    }

    private void writeNodeToLines(TreeNode node, List<String> lines, int depth) {
        if (depth > 0) {
            lines.add(node.name);
        }
        lines.addAll(node.configLines);
        for (TreeNode childNode : node.nodes) {
            writeNodeToLines(childNode, lines, depth + 1);
        }
        if (node.enableLineBreak) {
            lines.add("");
        }
    }
}



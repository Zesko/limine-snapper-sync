package org.limine.snapper.processes;

import org.limine.snapper.formats.limine8.LimineKey;
import org.limine.snapper.objects.*;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.*;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    private static final Scanner INPUT_SCANNER = new Scanner(System.in);
    private static final int MACHINE_ID_LENGTH = 32;
    private static final String MACHINE_ID_FILE_PATH = "/etc/machine-id";
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter ISO_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
    // Regex for forbidden escape sequences and characters
    private static final Pattern FORBIDDEN_PATTERN = Pattern.compile(
            "\\x1B\\[[A-Za-z]|\n"  // Arrow key escape sequences (e.g., \x1B[A for Up Arrow)
    );


    /**
     * Check if you use Btrfs?
     *
     * @return if you don't use Btrfs, then return false.
     */
    public static boolean areYouInBtrfs() {
        Output output = getTextFromCommand("findmnt --mountpoint / -no FSTYPE", false, "");
        if (output.isSuccess()) {
            return output.text().get(0).toLowerCase().contains("btrfs");
        } else {
            String errMessage = "Command line fails: 'findmnt --mountpoint / -no FSTYPE'";
            errorMessage(errMessage);
            return false;
        }
    }


    /**
     * Checks if a snapshot path exists in the /proc/cmdline command.
     *
     * @return true, if a snapshot path is found, otherwise false.
     */
    public static boolean areYouInSnapshot() {
        String snapshotPattern = "subvol=.*?/([0-9]+)/snapshot";
        try (BufferedReader reader = new BufferedReader(new FileReader(Config.PROC_CMDLINE))) {
            String line = reader.readLine();
            if (line != null) {
                Pattern pattern = Pattern.compile(snapshotPattern);
                Matcher matcher = pattern.matcher(line);
                return matcher.find();
            }
        } catch (IOException ignored) {
        }
        return false;
    }


    /**
     * Ask user to choose "yes", "no" or "cancel"
     */
    public static boolean askUser(String ask) {
        // Read user input from the console
        boolean isYes = false;
        while (true) {
            // Ask the user to provide something
            System.out.print(ask + ConsoleColor.YELLOW + "\nEnter your choice: " + ConsoleColor.RESET);
            // Read the entire entered line
            String userInput = INPUT_SCANNER.nextLine();
            if (userInput.trim().isEmpty()) {
                System.err.println(ConsoleColor.RED + "The input should not be empty." + ConsoleColor.RESET + userInput);
            } else if ("yes".startsWith(userInput.toLowerCase())) {
                isYes = true;
                break;
            } else if ("no".startsWith(userInput.toLowerCase())) {
                break;
            } else if ("cancel".startsWith(userInput.toLowerCase())) {
                System.out.println("The process is aborted.");
                System.exit(1);
                isYes = false;
            } else {
                System.err.println(ConsoleColor.RED + "The unknown input: " + ConsoleColor.RESET + userInput);
            }
        }
        return isYes;
    }


    /**
     * Build a path.
     */
    public static String buildPath(String... parts) {
        String path = Paths.get("", parts).toString();
        if (path.startsWith(File.separator)) {
            return path;
        } else {
            return File.separator + path;
        }
    }


    /**
     * Calculate a hash code of a chosen file.
     */
    public static String calculateHash(String absoluteFilePath) {
        return checkForHashCorruption(absoluteFilePath, Config.HASH_FUNCTION, 2);
    }

    /**
     * Calculate a hash code of a chosen file.
     */
    private static String calculateHash(String absoluteFilePath, Hash hashFunction) {
        Output output = Utility.getTextFromCommand(hashFunction.command + " " + absoluteFilePath, false, "Hash calculation failed, because the file: " + absoluteFilePath + " does not exist.");
        if (output.isSuccess()) {
            return Utility.extractHash(output.text().get(0));
        } else {
            return Config.NO_FILE_NAME;
        }
    }

    /**
     * Calculate the hash of a file using two parallel threads and check for hash corruption.
     * If corruption is detected, the process is repeated a specified number of times.
     *
     * @param absoluteFilePath  The path of the file to calculate the hash for.
     * @param hashFunction      The hashing function to use.
     * @param repeatIfCorrupted The number of times to retry if corruption is detected.
     * @return The final hash if no corruption is detected.
     */
    public static String checkForHashCorruption(String absoluteFilePath, Hash hashFunction, int repeatIfCorrupted) {
        int attempts = 0;
        while (attempts <= repeatIfCorrupted) {
            try {
                // Run hash calculation asynchronously in two threads using CompletableFuture
                CompletableFuture<String> hashFuture1 = CompletableFuture.supplyAsync(() -> calculateHash(absoluteFilePath, hashFunction));
                CompletableFuture<String> hashFuture2 = CompletableFuture.supplyAsync(() -> calculateHash(absoluteFilePath, hashFunction));

                // Get the hash results from both futures
                String hashResult1 = hashFuture1.get();
                String hashResult2 = hashFuture2.get();

                // Compare the results
                if (hashResult1.equals(hashResult2)) {
                    return hashResult1;  // Return the hash if they match (no corruption detected)
                } else {
                    // Log and print an error message if corruption is detected
                    String errMessage = String.format(
                            "Hash mismatch detected! Do not trust your hardware! Corruption suspected for file: %s (Attempt %d/%d).",
                            absoluteFilePath, attempts + 1, repeatIfCorrupted + 1
                    );
                    errorMessage(errMessage);
                    loggerSend(errMessage, LogLevel.CRITICAL);

                    if (attempts == repeatIfCorrupted) {
                        // If max attempts reached, return the first result and log a critical error
                        errMessage = "Max retry attempts reached. Returning potentially corrupted hash. Do not trust your hardware!";
                        errorMessage(errMessage);
                        return hashResult1;
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Error occurred while calculating the hash.", e);
            }
            attempts++;
        }
        // Should not reach this point, but return the last result as a fallback
        throw new IllegalStateException("Unexpected state in hash calculation.");
    }

    private static String extractHash(String outputLine) {
        StringBuilder hash = new StringBuilder();
        outputLine = outputLine.trim();
        for (int i = 0; i < outputLine.length(); i++) {
            char c = outputLine.charAt(i);
            if (c == ' ') {
                break;
            }
            hash.append(c);
        }
        return hash.toString();
    }


    /**
     * Change POSIX permissions of a file.
     */
    public static void changeFilePermissions(String filePath, String permissions) {
        Path path = Paths.get(filePath);
        // Convert the permission string to a set of PosixFilePermissions
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString(permissions);
        try {
            Files.setPosixFilePermissions(path, perms);
            infoMessage("Permissions " + permissions + " changed successfully for file:", filePath);
        } catch (IOException e) {
            String errMessage = "Failed to change permissions " + permissions + " for file:" + filePath;
            errorMessage(errMessage);
        }
    }


    /**
     * Remove leading symbols in the name.
     */
    public static String cleanName(String name) {
        if (name == null || name.isEmpty()) {
            return name;
        }
        name = name.trim();
        int i = 0;
        boolean isColonFound = false;
        // Loop to skip all leading colons
        while (i < name.length() && name.startsWith(LimineKey.ENTRY_KEY.toString(), i)) {
            isColonFound = true;
            i++;
        }
        if (isColonFound && name.startsWith(LimineKey.EXPAND.toString(), i)) {
            i++;
        }
        return name.substring(i);
    }


    /**
     * Get list information about all files from the directory.
     */
    public static List<String> collectFilesFromDirectory(String directoryPath) {
        List<String> list = new ArrayList<>();
        Path path = Paths.get(directoryPath);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                String fileName = entry.getFileName().toString();
                list.add(fileName);
            }
        } catch (IOException ignored) {
            errorMessage("The directory cannot be listed:", directoryPath);
        }
        return list;
    }


    /**
     * Compare two timestamps.
     *
     * @return If the value is less than 0, timestamp A is before timestamp B.
     * If it is greater than 0, timestamp A is after timestamp B.
     * If it is 0, both are the same.
     */
    public static int compareTimestamps(String timestampA, String timestampB) {
        return timestampA.compareTo(timestampB);
    }


    /**
     * Convert UTC time to local time.
     */
    public static String convertUTCToLocal(String utcTime) {
        // Parsing UTC time directly to ZonedDateTime in UTC
        LocalDateTime utcLdt = LocalDateTime.parse(utcTime, TIME_FORMATTER);
        // Convert to local time using system default time zone
        ZonedDateTime localZdt = utcLdt.atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault());
        // Return formatted local time
        return TIME_FORMATTER.format(localZdt);
    }


    /**
     * Copy a file.
     */
    public static void copyFile(String sourceFilePath, String targetFilePath, boolean force) {
        copyFile(sourceFilePath, targetFilePath, force, true, true);
    }


    /**
     * Copy a file.
     */
    public static void copyFile(String sourceFilePath, String targetFilePath, boolean force, boolean showOutput, boolean showCommand) {
        if (force) {
            Utility.runCommand("cp -f '" + sourceFilePath + "' '" + targetFilePath + "'", showOutput, showCommand);
        } else {
            Utility.runCommand("cp -u '" + sourceFilePath + "' '" + targetFilePath + "'", showOutput, showCommand);
        }
    }


    /**
     * Delete a file.
     */
    public static void deleteFile(String absoluteFilePath) {
        deleteFile(absoluteFilePath, true, true);
    }

    /**
     * Delete a file.
     */
    public static void deleteFile(String absoluteFilePath, boolean showOutput, boolean showCommand) {
        Utility.runCommand("rm -f '" + absoluteFilePath + "'", showOutput, showCommand);
    }


    /**
     * Check whether the command exists.
     */
    public static boolean isCommandPresent(String command) {
        // Get the PATH environment variable
        String pathEnv = System.getenv("PATH");
        if (pathEnv == null || pathEnv.isEmpty()) {
            return false; // If the PATH is empty, the command cannot be found
        }

        // Split the PATH by the system's path separator (":" on Linux/Unix)
        String[] paths = pathEnv.split(File.pathSeparator);

        // Iterate through each directory in PATH
        for (String path : paths) {
            // Check if the command exists in the current directory
            File commandFile = new File(path, command);
            if (commandFile.isFile() && commandFile.canExecute()) {
                return true; // Command exists and is executable
            }
        }
        return false; // Command isn't found in any PATH directories
    }


    /**
     * Check if the directory is present?
     */
    public static boolean isDirectoryPresent(String dirPath) {
        Path path = Paths.get(dirPath);
        try {
            // Check if the path exists and is a directory (following symbolic links)
            return Files.exists(path) && Files.isDirectory(path);
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * Verifies if the file is older than the specified number of hours.
     *
     * @param filePath Path to the file
     * @param hours    the number of hours to compare
     * @return true if the file is older than the specified hours, false otherwise.
     */
    public static boolean isFileOlderThan(String filePath, int hours) {
        File file = new File(filePath);
        long lastModified = file.lastModified();
        if (lastModified == 0L) {
            // File does not exist or timestamp is invalid
            return false;
        }
        long now = System.currentTimeMillis();
        long diffMillis = now - lastModified;
        long thresholdMillis = hours * 3600_000L;
        return diffMillis > thresholdMillis;
    }


    /**
     * Check if the file is present?
     */
    public static boolean isFilePresent(String filePath) {
        File file = new File(filePath);
        return file.exists() && file.isFile();
    }


    /**
     * Check if a time format is valid?
     * Note: If the snapper list and all snapshot entries do not have the same time format,
     * this will lead to all snapshot entries in the bootloader being wiped out.
     * That is why this method is important to prevent this issue.
     */
    public static boolean isTimeFormatNotValid(String timeString) {
        try {
            LocalDateTime.parse(timeString, TIME_FORMATTER);
            return false;
        } catch (DateTimeParseException e) {
            return true;
        }
    }


    /**
     * Checks whether the specified string represents a valid UUID.
     *
     * @param uuidString the string to be checked
     * @return true, if the string is a valid UUID, otherwise false
     */
    public static boolean isValidUUID(String uuidString) {
        if (uuidString == null || uuidString.isEmpty()) {
            return false;
        }
        try {
            UUID uuid = UUID.fromString(uuidString);
            return uuidString.equals(uuid.toString());
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


    /**
     * Make sure that the directory exists. If it does not exist, it will be created.
     */
    public static boolean ensureDirectoryExists(String dirPath) {
        // Construct the full directory path
        File dir = new File(dirPath);
        // Check if the directory exists
        if (!dir.exists()) {
            // If it doesn't exist, create the directory
            if (dir.mkdirs()) {
                infoMessage("Directory created:", dir.getPath());
                return true;
            } else {
                errorMessage("Failed to create directory:", dir.getPath());
                return false;
            }
        }
        return true;
    }


    /**
     * Make sure that the path exists. If it does not exist, it will be created.
     */
    public static void ensurePathExists(String filePath) {
        Path path = Paths.get(filePath);
        if (Files.exists(path)) {
            return;
        }
        ensureDirectoryExists(path.getParent().toString());
    }

    public static void infoMessage(String message) {
        infoMessage(message, null);
    }

    public static void infoMessage(String message, String output) {
        if (output == null || output.isEmpty()) {
            System.out.println(ConsoleColor.WHITE_BOLD + message + ConsoleColor.RESET);
        } else {
            System.out.println(ConsoleColor.GREEN_BOLD + message + ConsoleColor.RESET + " " + output);
        }
    }

    public static void warnMessage(String message) {
        warnMessage(message, null);
    }

    public static void warnMessage(String message, String output) {
        if (output == null || output.isEmpty()) {
            System.out.println(ConsoleColor.YELLOW_BOLD + message + ConsoleColor.RESET);
        } else {
            System.out.println(ConsoleColor.YELLOW_BOLD + message + ConsoleColor.RESET + " " + output);
        }
    }

    public static void errorMessage(String message) {
        errorMessage(message, null);
    }

    public static void errorMessage(String message, String output) {
        if (output == null || output.isEmpty()) {
            System.err.println(ConsoleColor.RED_BOLD + message + ConsoleColor.RESET);
            Utility.loggerSend(message, LogLevel.ERROR);
        } else {
            System.err.println(ConsoleColor.RED_BOLD + message + ConsoleColor.RESET + " " + output);
            Utility.loggerSend(message + " " + output, LogLevel.ERROR);
        }
    }

    public static void exitWithError(String message) {
        exitWithError(message, null, false);
    }

    public static void exitWithError(String message, boolean hold) {
        exitWithError(message, null, hold);
    }

    public static void exitWithError(String message, String output) {
        exitWithError(message, output, false);
    }

    public static void exitWithError(String message, String output, boolean hold) {
        errorMessage(message, output);
        if (hold || Config.IS_TIME_FOR_RESTORE) {
            // Prevent any terminal GUI from closing automatically on restore failure.
            infoMessage("You can review, copy or take a screenshot of this error output before the terminal closes.", null);
            // Remove the lock files in /tmp
            Utility.deleteFile(Config.LOCK_RESTORE_FILE, false, false);
            // Prevent any terminal GUI from closing automatically on restore failure.
            while (!Utility.askUser("Do you want to close this terminal? (yes|no) If no, just ignore this question in loop.")) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
            }
        }
        System.exit(1);
    }


    /**
     * Get a parent directory path.
     */
    public static String extractDirectoryPath(String path) {
        int lastIndex = path.lastIndexOf('/');
        if (lastIndex != -1) {
            return path.substring(0, lastIndex) + "/";
        } else {
            // If no '/' is found, return ""
            return "";
        }
    }


    /**
     * Get one last word from the line of text.
     */
    public static String extractLastWord(String line) {
        // Find the last space in the line
        int lastSpaceIndex = line.lastIndexOf(' ');
        // Extract the last word
        if (lastSpaceIndex != -1) {
            return line.substring(lastSpaceIndex + 1).trim();
        } else {
            // If there's no space, the whole line is one word
            return line;
        }
    }


    /**
     * Get the latest ID of Snapper snapshot.
     */
    public static int findLatestSnapperNumber(String directoryPath) {
        Path path = Paths.get(directoryPath);
        int maxNumber = -1;
        Pattern pattern = Pattern.compile("\\d+"); // Regex to match directory names that are numbers

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                String fileName = entry.getFileName().toString();
                if (pattern.matcher(fileName).matches()) {
                    int number = Integer.parseInt(fileName);
                    if (number > maxNumber) {
                        maxNumber = number;
                    }
                }
            }
        } catch (IOException ignored) {
            String errMessage = "The latest Snapper snapshot number cannot be found in the directory: " + directoryPath;
            errorMessage(errMessage);
        }
        return maxNumber;
    }


    /**
     * Generate a machine-ID.
     */
    public static String generateRandomMachineID() {
        SecureRandom random = new SecureRandom();
        StringBuilder newMachineID = new StringBuilder(MACHINE_ID_LENGTH);
        for (int i = 0; i < MACHINE_ID_LENGTH; i++) {
            int nextInt = random.nextInt(16); // 0-15 for hexadecimal
            newMachineID.append(Integer.toHexString(nextInt));
        }
        try {
            Files.writeString(Paths.get(MACHINE_ID_FILE_PATH), newMachineID);
            infoMessage("New machine-id generated:", newMachineID.toString());
            infoMessage("This machine-id saved:", MACHINE_ID_FILE_PATH);
            changeFilePermissions(MACHINE_ID_FILE_PATH, "r--r--r--");
        } catch (IOException e) {
            exitWithError("New machine ID generator failed. Error:", e.getMessage());
        }
        return newMachineID.toString();
    }


    /**
     * Get the current ISO time now.
     */
    public static String getCurrentIsoFormattedTime() {
        return LocalDateTime.now().withNano(0).format(ISO_FORMATTER);
    }


    /**
     * Get the current UTC time now
     */
    public static String getCurrentUTCTime() {
        return LocalDateTime.now(ZoneOffset.UTC).format(TIME_FORMATTER);
    }


    /**
     * Extracts the leading spaces from a given string.
     *
     * @param input The input string from which leading spaces are to be extracted.
     * @return A string containing only the leading spaces.
     */
    public static String getLeadingSpaces(String input) {
        if (input == null) {
            return "";
        }
        // Initialize a StringBuilder to store the leading spaces
        StringBuilder leadingSpaces = new StringBuilder();
        int length = input.length();

        // Iterate through the string to collect leading spaces
        for (int i = 0; i < length; i++) {
            if (input.charAt(i) == ' ') {
                leadingSpaces.append(' ');
            } else {
                // Stop when a non-space character is found
                break;
            }
        }
        return leadingSpaces.toString();
    }


    public static String getSubvolFromProcMounts(String mount) {
        Path procPath = Path.of("/proc/mounts");
        String prefix = "subvol=";
        try (BufferedReader reader = Files.newBufferedReader(procPath)) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.contains(" " + mount + " ")) {
                    continue;
                }
                String[] parts = line.split(" ");
                if (parts.length < 4 || !parts[1].equals(mount)) {
                    continue;
                }
                String options = parts[3];
                int idx = options.indexOf(prefix);
                if (idx != -1) {
                    int start = idx + prefix.length();
                    int end = options.indexOf(',', start);
                    return (end == -1) ? options.substring(start) : options.substring(start, end);
                }
            }
        } catch (IOException ignored) {
        }
        return null;
    }


    /**
     * Get the result from a command output using ProcessBuilder.
     *
     * @param commandLine  The command to be executed.
     * @param exitIfError  If true, the application will exit in case of error.
     * @param errorMessage Custom error message to display/log in case of error.
     * @return List of strings containing the output of the command.
     */
    public static Output getTextFromCommand(String commandLine, boolean exitIfError, String errorMessage) {
        // Use ProcessBuilder to create and start the process
        ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", commandLine)
                .redirectErrorStream(true);// Redirect error stream to the standard output stream
        try {
            Process process = processBuilder.start();
            List<String> textOutput = new ArrayList<>();

            // Use try-with-resources to automatically close BufferedReader
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                reader.lines().forEach(textOutput::add);
            }

            // Wait for the process to finish and get the exit code
            int errorCode = process.waitFor();
            if (errorCode == 0) {
                return new Output(textOutput, null, errorCode, true);
            } else {
                if (errorMessage != null) {
                    if (!errorMessage.isEmpty()) {
                        errorMessage(errorMessage);
                    }
                } else {
                    errorMessage("Command failed:", commandLine);
                    if (!textOutput.isEmpty()) {
                        if (!textOutput.get(0).trim().isEmpty()) {
                            errorMessage("Output:", Arrays.toString(textOutput.toArray()));
                        }
                    }
                }
                if (exitIfError) {
                    exitWithError("Exit code:", String.valueOf(errorCode));
                }
                return new Output(null, textOutput, errorCode, false);
            }

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Get info about ESP usage.
     */
    public static double getUsagePercent(String mountPoint) {
        Path path = Paths.get(mountPoint);
        try {
            FileStore store = Files.getFileStore(path);
            long total = store.getTotalSpace();// Total size in bytes
            long usable = store.getUsableSpace();
            long used = total - usable;
            double percent = (double) used / total * 100;
            return new BigDecimal(percent)
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
        } catch (IOException e) {
            errorMessage("Error when checking the usage of " + mountPoint + ":", e.getMessage());
            return -1;
        }
    }


    /**
     * Read UUID.
     */
    public static String getUuid() {
        Output output = getTextFromCommand("findmnt --mountpoint / -no UUID", true, null);
        if (output.isSuccess()) {
            if (!output.text().isEmpty() && !output.text().get(0).isEmpty()) {
                return output.text().get(0);
            }
        }
        return null;
    }


    /**
     * Get version.
     */
    public static String getVersion() {
        try (InputStream manifestStream = Utility.class.getResourceAsStream("/META-INF/MANIFEST.MF")) {
            if (manifestStream != null) {
                Manifest manifest = new Manifest(manifestStream);
                Attributes attributes = manifest.getMainAttributes();
                return attributes.getValue("Implementation-Version");
            }
        } catch (Exception ignored) {
        }
        return "Unknown";
    }


    /**
     * Predict if a partition space is enough when adding new files?
     */
    public static boolean doesPartitionHaveSpace(double limitUsagePercent, String mountPoint, Set<String> filePaths, Set<String> deletedFilePaths) {
        try {
            // Calculate the total size of the new files
            long newFilesSize = calculateFilesSize(filePaths, deletedFilePaths);
            // Determine the FileStore for the specified ESP/partition path
            FileStore store = Files.getFileStore(Paths.get(mountPoint));
            long totalSize = store.getTotalSpace();
            long currentUsage = totalSize - store.getUsableSpace();
            // Combined usage = previous usage + size of new files + 4 MiB (snapshots.json + limine.conf + deviation)
            long combinedUsage = currentUsage + newFilesSize + 4194304;
            double usagePercent = (double) combinedUsage / totalSize * 100;
            // Return true if the combined usage is less than the limit usage.
            return usagePercent < limitUsagePercent;
        } catch (IOException e) {
            exitWithError(e.getMessage());
            return false;
        }
    }

    private static long calculateFilesSize(Set<String> filePaths, Set<String> deletedFilePaths) {
        long newFilesSize = 0;
        File file;
        for (String filePath : filePaths) {
            file = new File(filePath);
            if (file.exists() && file.isFile()) {
                newFilesSize += file.length();
            }
        }
        for (String filePath : deletedFilePaths) {
            file = new File(filePath);
            if (file.exists() && file.isFile()) {
                newFilesSize -= file.length();
            }
        }
        return newFilesSize;
    }


    /**
     * Logger
     */
    public static synchronized void loggerSend(String message, LogLevel logLevel) {
        try {
            // Bugfix: Do not use Runtime.getRuntime() that has a bug to escape some character like '-'
            // Use ProcessBuilder instead.
            ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", "logger -t limine-snapper-sync -p " + logLevel.level + " \"" + message + "\"");
            processBuilder.start();
        } catch (IOException ignored) {
        }
    }


    /**
     * Move a file.
     */
    public static void moveFile(String sourceFilePath, String targetFilePath, boolean force) {
        if (force) {
            Utility.runCommand("mv -f '" + sourceFilePath + "' '" + targetFilePath + "'", true, true);
        } else {
            Utility.runCommand("mv -u '" + sourceFilePath + "' '" + targetFilePath + "'", true, true);
        }
    }


    /**
     * Prints a snapper table with a nice format for the output in the terminal.
     */
    public static void printTable(List<String[]> data, int columnWidth1, int columnWidth2, int columnWidth3) {

        String colSep = "│", rowColSep = "┼", rowSep = "─";
        // Here you can define the width of the columns
        int[] columnWidths = {columnWidth1, columnWidth2, columnWidth3};

        // Print header
        String header = String.format(" %1$-" + columnWidths[0] + "s " + colSep + " %2$-" + columnWidths[1] + "s " + colSep + " %3$-" + columnWidths[2] + "s ", "ID", "Date", "Description");
        String rowSeparator = rowSep.repeat(columnWidths[0] + 2) + rowColSep + rowSep.repeat(columnWidths[1] + 2) + rowColSep + rowSep.repeat(columnWidths[2] + 2);

        System.out.println(header);
        System.out.println(rowSeparator);

        // Print lines
        for (String[] row : data) {
            System.out.printf(" %1$-" + columnWidths[0] + "s " + colSep + " %2$-" + columnWidths[1] + "s " + colSep + " %3$-" + columnWidths[2] + "s %n", row[0], row[1], row[2]);
        }
    }


    /**
     * Read a machine ID in /etc/machine-id.
     * Note: If it does not exist, a new machine-ID will be generated.
     */
    public static String readMachineID() {
        String machineID;
        try {
            machineID = new String(Files.readAllBytes(Paths.get("/etc/machine-id"))).trim();
        } catch (IOException e) {
            machineID = generateRandomMachineID();
        }
        return machineID;
    }


    /**
     * Remove the surrounding splashes from a path.
     */
    public static String removeSurroundingSlashes(String path) {
        int start = 0;
        int end = path.length();
        while (start < end && path.charAt(start) == '/') {
            start++;
        }
        while (end > start && path.charAt(end - 1) == '/') {
            end--;
        }
        return path.substring(start, end).trim();
    }


    /**
     * Run a single command.
     */
    public static boolean runCommand(String command, boolean showOutput, boolean showCommand) {
        return runCommands(List.of(command), true, showOutput, showCommand);
    }


    /**
     * Run multiple commands.
     */
    public static boolean runCommands(List<String> commands, boolean stopNextCommandOnError, boolean showOutput, boolean showCommand) {
        boolean isSuccess = true;
        try {
            for (String command : commands) {
                ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);
                Process process = processBuilder.start();

                CompletableFuture<Void> outputFuture = null;
                CompletableFuture<Void> errorFuture = null;

                if (showOutput) {
                    // Use CompletableFuture to asynchronously read output and error streams
                    outputFuture = CompletableFuture.runAsync(() -> printStream(process.getInputStream(), System.out::println));
                    errorFuture = CompletableFuture.runAsync(() -> printStream(process.getErrorStream(), System.err::println));
                }
                // Wait for the process to complete
                int errorCode = process.waitFor();

                if (showOutput) {
                    // Wait for the output/error streams to finish processing
                    outputFuture.join();
                    errorFuture.join();
                }
                if (errorCode != 0) {
                    isSuccess = false;
                    errorMessage("Command failed:", "\"" + command + "\" with exit code: " + errorCode);
                    if (stopNextCommandOnError) {
                        // Stop continue to next command
                        break;
                    }
                } else {
                    if (showCommand) {
                        infoMessage("Command succeeded:", command);
                    }
                }
            }
            return isSuccess;
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static void printStream(InputStream inputStream, Consumer<String> consumer) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            bufferedReader.lines().forEach(consumer);
        } catch (IOException ignored) {
            errorMessage("Error reading process stream");
        }
    }

    /**
     * Find out what Snapper config name is for the root subvolume.
     */
    public static String snapperConfigName() {
        Output output = getTextFromCommand(Config.SNAPPER_COMMAND + " list-configs", false, null);
        if (output.isSuccess()) {
            for (String name : output.text()) {
                if (name.trim().endsWith("/")) {
                    String[] columns = name.split(",");
                    if (columns.length == 2) {
                        if ("/".equals(columns[1].trim())) {
                            return columns[0].trim();
                        }
                    }
                }
            }
        }
        return null;
    }


    /**
     * User input in the terminal.
     */
    public static String userInput(String message) {
        System.out.println(message);
        while (true) {
            System.out.print(ConsoleColor.YELLOW + "\nEnter your input: " + ConsoleColor.RESET);
            // Read the entire entered line
            String userInput = INPUT_SCANNER.nextLine().trim();
            if (isValidInput(userInput)) {
                return userInput;
            } else {
                System.err.println(ConsoleColor.RED + "Invalid input. Please avoid using escape sequences and characters." + ConsoleColor.RESET);
            }
        }
    }

    private static boolean isValidInput(String input) {
        // Check for forbidden escape sequences and characters
        return !FORBIDDEN_PATTERN.matcher(input).find();
    }


    /**
     * Get user input.
     */
    public static String userSelect(String ask) {
        // Ask the user to provide something
        System.out.print(ask + ConsoleColor.YELLOW + "\nEnter your choice: " + ConsoleColor.RESET);
        String input = INPUT_SCANNER.nextLine();
        if (!input.trim().isEmpty() && "cancel".startsWith(input.toLowerCase())) {
            System.out.println("The process is aborted.");
            System.exit(1);
        }
        // Read the entire entered line
        return input;
    }


    /**
     * Get a valid snapshot ID.
     *
     * @return -1 is invalid, otherwise valid.
     */
    public static int validateAndGetSnapshotID() {
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Config.PROC_CMDLINE));
            line = reader.readLine();
        } catch (IOException e) {
            line = null;
        }

        int snapshotID = -1;
        boolean isRootFlagsFound = false;
        if (line != null) {
            // Check if the keyword "rootflags" exists in the output.
            String[] kernelParameters = line.split(" ");
            for (String parameter : kernelParameters) {
                // Split the rootflags configuration part into individual flags
                String[] rootflags = readRootFlags(parameter);
                if (rootflags != null) {
                    for (String flag : rootflags) {
                        // Check if the flag contains the "snapshot" keyword
                        if (flag.startsWith("subvol=") && flag.endsWith("/snapshot")) {
                            // Extract the snapshot ID from the flag
                            snapshotID = extractSnapshotID(flag);
                            break;
                        }
                    }
                    isRootFlagsFound = true;
                    break;
                }
            }
        } else {
            String errMessage = "No kernel cmdline from " + Config.PROC_CMDLINE;
            errorMessage(errMessage);
            return -1;
        }

        if (!isRootFlagsFound) {
            // You are probably in a chroot environment that does not have 'rootflags'
            return -1;
        }
        return snapshotID;
    }

    private static int extractSnapshotID(String flag) {
        try {
            String[] dirs = flag.split("/");
            String snapshotIDStr = dirs[dirs.length - 2];
            if (snapshotIDStr != null) {
                return Integer.parseInt(snapshotIDStr);
            }
        } catch (NumberFormatException ignored) {
        }
        String errMessage = "Failed to extract snapshot ID from the flag: " + flag;
        exitWithError(errMessage);
        return -1;
    }

    public static String[] readRootFlags(String rootFlags) {
        if (rootFlags.startsWith("rootflags=")) {
            String rootflagsPart = rootFlags.substring("rootflags=".length());
            // Split the rootflags configuration part into individual flags
            return rootflagsPart.split(",");
        }
        return null;
    }
}

package org.limine.snapper.processes;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;


public class SnapperWriter {

    /**
     * Write a new info XML about the Snapper snapshot.
     */
    public static void writeInfoXml(String snapshotID, String utcTime, String text, String filePath) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element root = document.createElement("snapshot");
            document.appendChild(root);

            Element type = document.createElement("type");
            type.appendChild(document.createTextNode("single"));
            root.appendChild(type);

            Element num = document.createElement("num");
            num.appendChild(document.createTextNode(snapshotID));
            root.appendChild(num);

            Element date = document.createElement("date");
            date.appendChild(document.createTextNode(utcTime));
            root.appendChild(date);

            Element description = document.createElement("description");
            description.appendChild(document.createTextNode(text));
            root.appendChild(description);

            //Element cleanup = document.createElement("cleanup");
            //cleanup.appendChild(document.createTextNode("number"));
            //root.appendChild(cleanup);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            // Enable beautiful formatting
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            // Set XML declaration as in the original (Not work)
//            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
//            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
//            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//            transformer.setOutputProperty(OutputKeys.STANDALONE, "no");

            // Creating the DOM source and stream result
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filePath));

            // Transform the document into the file
            transformer.transform(domSource, streamResult);
            Utility.infoMessage("Saved successfully:", filePath);
        } catch (ParserConfigurationException | TransformerException e) {
            String errMessage = "Error writing file: " + e.getMessage();
            Utility.errorMessage(errMessage);
        }
    }
}

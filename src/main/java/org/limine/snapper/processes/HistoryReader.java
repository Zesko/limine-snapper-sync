package org.limine.snapper.processes;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.limine.snapper.formats.json1.*;
import org.limine.snapper.formats.snapper.SnapperProperty;
import org.limine.snapper.objects.Config;
import org.limine.snapper.objects.ConsoleColor;
import org.limine.snapper.objects.Hash;
import org.limine.snapper.objects.Output;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HistoryReader {

    public static LimineHistory load(String filePath) {
        if (Utility.isFilePresent(filePath)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE, true);
            try {
                return mapper.readValue(new File(filePath), LimineHistory.class);
            } catch (IOException e) {
                String errMessage = "The JSON format is broken or incompatible:";
                Utility.errorMessage(errMessage, e.getMessage());
                if (Utility.isFilePresent(filePath + Config.BACKUP_SUFFIX)) {
                    Utility.errorMessage("Then loading the old file from the backup:", filePath + Config.BACKUP_SUFFIX);
                    return load(filePath + Config.BACKUP_SUFFIX);
                } else {
                    System.exit(1);
                    return null;
                }
            }

        } else {
            Utility.ensurePathExists(filePath);
            // Create a new LimineHistory with default values.
            LimineHistory limineHistory = new LimineHistory();
            limineHistory.setJsonFormatVersion(Config.JSON_FORMAT_VERSION);
            limineHistory.setLastSnapshotID(0);
            limineHistory.setUuid(null);
            limineHistory.setLastTimestamp(Config.DEFAULT_TIMESTAMP);
            return limineHistory;
        }
    }

    public static HashSet<String> displaySnapshotList(LimineHistory limineHistory) {
        System.out.println();
        List<String[]> data = new ArrayList<>();
        HashSet<String> IDs = new HashSet<>();
        int columnWidth1 = "ID".length(), columnWidth2 = "Timestamp".length(), columnWidth3 = "Description".length();
        for (SnapshotEntry snapshot : limineHistory.getSnapshotEntries()) {
            SnapperEntry entry = snapshot.snapperEntry();
            String id = String.valueOf(entry.snapshotID());
            IDs.add(id);
            if (columnWidth1 < id.length()) {
                columnWidth1 = id.length();
            }
            if (columnWidth2 < entry.timestamp().length()) {
                columnWidth2 = entry.timestamp().length();
            }
            String description = entry.properties().get(SnapperProperty.DESCRIPTION.replace);
            if (columnWidth3 < description.length()) {
                columnWidth3 = description.length();
            }
            String[] line = new String[]{id, entry.timestamp(), description};
            data.add(line);
        }
        // print a snapshots-table
        Utility.printTable(data, columnWidth1, columnWidth2, columnWidth3);
        return IDs;
    }

    public static void displayInfo(LimineHistory limineHistory, Config config) {
        System.out.println();
        // Display a detail:
        printFormattedInfo("Version", Config.TOOL_NAME + " " + Utility.getVersion(), true, ConsoleColor.GREEN_BOLD);
        Output output = Utility.getTextFromCommand("limine --version", false, "");
        if (output.isSuccess()) {
            String limineVersion = extractVersion(output.text().get(0));
            if (limineVersion != null) {
                if (isSupportedLimineMajorVersion(limineVersion)) {
                    printFormattedInfo("Limine version", limineVersion + " is supported.", true, ConsoleColor.GREEN_BOLD);
                } else {
                    printFormattedInfo("Limine version", limineVersion + " is not supported.", true, ConsoleColor.RED_BOLD);
                }
            }
        }
        printFormattedInfo("JSON format version", limineHistory.getJsonFormatVersion(), true, ConsoleColor.GREEN_BOLD);
        String uuid = limineHistory.getUuid();
        if (uuid != null) {
            printFormattedInfo("UUID", limineHistory.getUuid(), true, ConsoleColor.GREEN_BOLD);
        } else {
            printFormattedInfo("UUID", "No UUID", true, ConsoleColor.RED_BOLD);
        }
        printFormattedInfo("Last snapshot", "ID: " + limineHistory.getLastSnapshotID() + ", date: " + limineHistory.getLastTimestamp(), true, ConsoleColor.GREEN_BOLD);
        printFormattedInfo("Total of snapshots", "" + limineHistory.getSnapshotEntries().size(), true, ConsoleColor.GREEN_BOLD);
        printFormattedInfo("ESP usage", Utility.getUsagePercent(config.espPath()) + "%", true, ConsoleColor.GREEN_BOLD);
        verifyFiles(limineHistory, config);
        System.out.println();
    }

    public static void verifyFiles(LimineHistory limineHistory, Config config) {
        List<String> unusedFiles = new ArrayList<>();
        List<String> missedFiles = new ArrayList<>();
        List<String> corruptedFiles = new ArrayList<>();

        HashSet<String> neededFiles = new HashSet<>();
        for (SnapshotEntry entry : limineHistory.getSnapshotEntries()) {
            for (KernelEntry kernelEntry : entry.kernelEntries()) {
                for (ImageDetail image : kernelEntry.imageDetails()) {
                    neededFiles.add(image.fileHashName());
                }
            }
        }

        String dirPath = Utility.buildPath(config.espPath(), config.machineID(), Config.HISTORY_FOLDER);
        List<String> directory = Utility.collectFilesFromDirectory(dirPath);

        for (String file : directory) {
            // Check if the file is unnecessary
            if (!neededFiles.contains(file) && !(Config.HISTORY_FILE + Config.BACKUP_SUFFIX).startsWith(file)) {
                unusedFiles.add(file);
            }
        }

        for (String file : neededFiles) {
            // Check if the necessary file is present
            if (directory.contains(file)) {
                // Verify the necessary file if it is ok
                String filePath = Utility.buildPath(dirPath, file);
                Hash hashFunction = Hash.getHash(file);
                if (hashFunction != null) {
                    String hashcode = Utility.checkForHashCorruption(filePath, hashFunction, 2);
                    if (!file.endsWith(hashcode)) {
                        corruptedFiles.add(file);
                    }
                } else {
                    corruptedFiles.add(file);
                }
            } else {
                missedFiles.add(file);
            }
        }
        if (unusedFiles.isEmpty()) {
            printFormattedInfo("Unused files", String.valueOf(0), true, ConsoleColor.GREEN_BOLD);
        } else {
            printFormattedInfo("Unused files", String.valueOf(unusedFiles.size()), true, ConsoleColor.YELLOW_BOLD);
            for (String file : unusedFiles) {
                printFormattedInfo("", file, false, ConsoleColor.YELLOW);
            }
        }

        if (missedFiles.isEmpty()) {
            printFormattedInfo("Missed files", String.valueOf(0), true, ConsoleColor.GREEN_BOLD);
        } else {
            printFormattedInfo("Missed files", String.valueOf(missedFiles.size()), true, ConsoleColor.RED_BOLD);
            for (String file : missedFiles) {
                printFormattedInfo("", file, false, ConsoleColor.RED);
            }
        }

        if (corruptedFiles.isEmpty()) {
            printFormattedInfo("Corrupted files", String.valueOf(0), true, ConsoleColor.GREEN_BOLD);
        } else {
            printFormattedInfo("Corrupted files", String.valueOf(corruptedFiles.size()), true, ConsoleColor.RED_BOLD);
            for (String file : corruptedFiles) {
                printFormattedInfo("", file, false, ConsoleColor.RED);
            }
        }
    }

    private static void printFormattedInfo(String label, String value, boolean showColon, ConsoleColor color) {
        int labelWidth = 20; // Adjust this to the desired width
        String formattedLabel = String.format("%-" + labelWidth + "s", label);
        if (showColon) {
            System.out.println(color + formattedLabel + ": " + ConsoleColor.RESET + value);
        } else {
            System.out.println(color + formattedLabel + "  " + ConsoleColor.RESET + value);
        }
    }

    /**
     * Method to extract version number from a given string
     */
    private static String extractVersion(String input) {
        // Define a regular expression pattern for matching version numbers
        Pattern versionPattern = Pattern.compile("\\b(\\d+\\.\\d+\\.\\d+)\\b");
        Matcher matcher = versionPattern.matcher(input);

        // Check if a match is found and return the version number
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            // Return a default value or handle the case when no version is found
            return null;
        }
    }

    private static boolean isSupportedLimineMajorVersion(String versionString) {
        // Regex to extract the major part of the version
        String regex = "^([0-9]+)\\.";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(versionString);

        if (matcher.find()) {
            try {
                // Extract a major version and convert to integer
                int majorVersion = Integer.parseInt(matcher.group(1));
                // Check if the major version is supported
                return Config.SUPPORTED_LIMINE_MAJOR_VERSIONS.contains(majorVersion);
            } catch (NumberFormatException e) {
                // Error parsing major version
                Utility.errorMessage("Invalid version format:", versionString);
            }
        } else {
            Utility.errorMessage("No valid version format found:", versionString);
        }
        return false;
    }

}

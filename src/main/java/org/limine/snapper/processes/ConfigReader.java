package org.limine.snapper.processes;

import org.limine.snapper.objects.Config;
import org.limine.snapper.objects.Hash;
import org.limine.snapper.objects.Output;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class ConfigReader {

    private final Properties properties;

    public ConfigReader() {
        this.properties = loadConfigFiles();
        // Set environment variables into properties
        //System.getenv().forEach(this.properties::setProperty);
    }

    private Properties loadConfigFiles() {
        Properties properties = new Properties();
        for (String configFilePath : Config.CONFIG_FILE_PATHS) {
            if (Utility.isFilePresent(configFilePath)) {
                try (FileInputStream fileInputStream = new FileInputStream(configFilePath)) {
                    properties.load(fileInputStream);
                } catch (IOException e) {
                    Utility.exitWithError("Failed to load config file:", configFilePath + " due to " + e.getMessage());
                    return null;
                }
            }
        }
        properties.forEach((key, value) -> {
            if (value instanceof String stringValue) {
                String trimmedValue = stringValue.replaceAll("\"", "").trim();
                properties.setProperty((String) key, trimmedValue);
            }
        });
        return properties;
    }


    public Config readConfig() {

        String targetOs = properties.getProperty("TARGET_OS_NAME");
        if (targetOs != null) {
            if (!targetOs.isBlank()) {
                Config.TARGET_OS_NAME = Utility.cleanName(targetOs);
            }
        }

        int maxSnapshots = Integer.MAX_VALUE;
        String maxSnapshotEntries = properties.getProperty("MAX_SNAPSHOT_ENTRIES");
        if (maxSnapshotEntries == null) {
            maxSnapshotEntries = properties.getProperty("LIMIT_NUMBER");
            if (maxSnapshotEntries != null) {
                String warnMessage = "In " + Config.DEFAULT_CONFIG_FILE_PATH + ", 'LIMIT_NUMBER' is deprecated. Use 'MAX_SNAPSHOT_ENTRIES' instead.\n";
                Utility.warnMessage(warnMessage);
            }
        }

        if (maxSnapshotEntries != null) {
            try {
                maxSnapshots = Integer.parseInt(maxSnapshotEntries);
            } catch (NumberFormatException ignored) {
                String errMessage = "Value of MAX_SNAPSHOT_ENTRIES should be a number and not a word!";
                Utility.exitWithError(errMessage);
                return null;
            }
        }

        String espPath = properties.getProperty("ESP_PATH");
        if (espPath == null) {
            if (Utility.isCommandPresent("bootctl")) {
                Output output = Utility.getTextFromCommand("bootctl --print-esp-path", false, null);
                if (output.isSuccess()) {
                    espPath = output.text().get(0);
                }
            }
        }
        if (espPath != null) {
            espPath = Utility.buildPath(espPath.trim());
            // Check if espPath is valid in your system, e.g., Does <ESP>/limine.conf exist?
            String limineConfigPath = Utility.buildPath(espPath, Config.LIMINE_CONFIG_FILE);
            if (!Utility.isFilePresent(limineConfigPath)) {
                String errMessage = "The file: " + limineConfigPath + " is not found.";
                Utility.exitWithError(errMessage);
                return null;
            }
        } else {
            String errMessage = "Please set ESP_PATH in " + Config.DEFAULT_CONFIG_FILE_PATH;
            Utility.exitWithError(errMessage);
            return null;
        }

        String rootSubvolumePath = properties.getProperty("ROOT_SUBVOLUME_PATH");
        // Please do not implement automatic detection of subvolume as this will not work when booting into a snapshot or overlayFs.
        if (rootSubvolumePath != null) {
            if (!rootSubvolumePath.isBlank()) {
                rootSubvolumePath = Utility.buildPath(rootSubvolumePath);
            } else {
                rootSubvolumePath = null;
            }
        }
        if (rootSubvolumePath == null) {
            String errMessage = "No ROOT_SUBVOLUME_PATH. Please set ROOT_SUBVOLUME_PATH in " + Config.DEFAULT_CONFIG_FILE_PATH;
            Utility.exitWithError(errMessage);
            return null;
        }

        String rootSnapshotsPath = properties.getProperty("ROOT_SNAPSHOTS_PATH");
        if (rootSnapshotsPath != null) {
            if (!rootSnapshotsPath.isBlank()) {
                rootSnapshotsPath = Utility.buildPath(rootSnapshotsPath);
            } else {
                rootSnapshotsPath = null;
            }
        }
        if (rootSnapshotsPath == null) {
            String errMessage = "No ROOT_SNAPSHOTS_PATH. Please set ROOT_SNAPSHOTS_PATH in " + Config.DEFAULT_CONFIG_FILE_PATH;
            Utility.exitWithError(errMessage);
            return null;
        }

        if (!Utility.isCommandPresent("snapper")) {
            String errMessage = "Snapper is not installed.";
            Utility.exitWithError(errMessage);
            return null;
        }


        String enableRsync = properties.getProperty("ENABLE_RSYNC_ASK");
        if (enableRsync != null) {
            if ("yes".equalsIgnoreCase(enableRsync)) {
                Config.ENABLE_RSYNC_ASK = true;
            }
        }

        String spaceNumber = properties.getProperty("SPACE_NUMBER");
        if (spaceNumber != null) {
            try {
                int number = Integer.parseInt(spaceNumber);
                StringBuilder spaces = new StringBuilder();
                while (number > 0) {
                    spaces.append(" ");
                    number--;
                }
                Config.SPACES = spaces.toString();
            } catch (NumberFormatException ignored) {
                String errMessage = "Value of SPACE_NUMBER should be a number and not a word!";
                Utility.errorMessage(errMessage);
            }
        }

        String limitUsagePercent = properties.getProperty("LIMIT_USAGE_PERCENT");
        if (limitUsagePercent != null) {
            try {
                double percent = Double.parseDouble(limitUsagePercent);
                if (0.0 < percent && percent < 100.0) {
                    Config.LIMIT_USAGE_PERCENT = percent;
                }
            } catch (NumberFormatException e) {
                String errMessage = "Value of LIMIT_USAGE_PERCENT should be from 1 to 99!";
                Utility.errorMessage(errMessage);
            }
        }

        String uuid = properties.getProperty("UUID");
        if (uuid != null) {
            if (Utility.isValidUUID(uuid)) {
                Config.UUID = uuid;
            }
        }

        String snapshotFormatChoice = properties.getProperty("SNAPSHOT_FORMAT_CHOICE");
        if (snapshotFormatChoice != null) {
            Config.SNAPSHOT_FORMAT_CHOICE = snapshotFormatChoice;
        }

        String hashName = properties.getProperty("HASH_FUNCTION");
        if (hashName != null) {
            Hash hashFunction = Hash.getHashFunction(hashName);
            if (hashFunction != null && Utility.isCommandPresent(hashFunction.command)) {
                Config.HASH_FUNCTION = hashFunction;
            } else {
                String errMessage = "The hash function '" + hashName + "' is not found or supported.";
                Utility.errorMessage(errMessage);
            }
        }

        String backupThreshold = properties.getProperty("BACKUP_THRESHOLD");
        if (backupThreshold != null) {
            try {
                Config.BACKUP_THRESHOLD = Integer.parseInt(backupThreshold);
            } catch (NumberFormatException e) {
                String errMessage = "BACKUP_THRESHOLD '" + backupThreshold + "' is invalid, it should be in hours format.";
                Utility.errorMessage(errMessage);
            }
        }

        String commands = properties.getProperty("COMMANDS_BEFORE_SAVE");
        if (commands != null) {
            Config.COMMANDS_BEFORE_SAVE = commands;
        }

        commands = properties.getProperty("COMMANDS_AFTER_SAVE");
        if (commands != null) {
            Config.COMMANDS_AFTER_SAVE = commands;
        }

        return new Config(Utility.readMachineID(), maxSnapshots, espPath, rootSubvolumePath, rootSnapshotsPath);
    }

    public void readSnapperConfig() {
        String snapperConfigName = properties.getProperty("SNAPPER_CONFIG_NAME");
        if (snapperConfigName != null) {
            if (!snapperConfigName.isBlank()) {
                Config.SNAPPER_CONFIG_NAME = snapperConfigName;
            } else {
                Config.SNAPPER_CONFIG_NAME = null;
            }
        }
        if (Config.SNAPPER_CONFIG_NAME == null) {
            Config.SNAPPER_CONFIG_NAME = Utility.snapperConfigName();
        }
        if (Config.SNAPPER_CONFIG_NAME == null && Utility.areYouInBtrfs()) {
            String warnMessage = "No Snapper config found for '/'. Creating a Snapper config named 'root' and single root snapshot.";
            Utility.warnMessage(warnMessage);
            // Automatically create a Snapper configuration and root snapshot if not present.
            Config.SNAPPER_CONFIG_NAME = initSnapperConfig();
        }
        if (Config.SNAPPER_CONFIG_NAME != null) {
            Config.SNAPPER_LIST_CMDLINE = Config.SNAPPER_COMMAND + " -c " + Config.SNAPPER_CONFIG_NAME + " list --disable-used-space --columns number,date,description";
        } else {
            String errMessage;
            if (Utility.areYouInBtrfs()) {
                errMessage = "No Snapper config found for '/'. Please configure Snapper and set SNAPPER_CONFIG_NAME in " + Config.DEFAULT_CONFIG_FILE_PATH + ".";
            } else {
                errMessage = "The '/' partition is not using Btrfs.";
            }
            Utility.exitWithError(errMessage);
        }
    }

    /**
     * Automatically create a Snapper configuration and root snapshot.
     */
    private String initSnapperConfig() {
        List<String> commands = List.of(
                "snapper -c root create-config /",
                "snapper -c root create --description 'limine-snapper-sync' --cleanup-algorithm number --read-only"
        );
        if (Utility.runCommands(commands, true, true, true)) {
            return "root";
        } else {
            return null;
        }
    }

    /**
     * Ensures that the mount paths are valid before taking a snapshot prior to restore.
     * In a chroot environment, the restore process cannot directly verify mount paths.
     * Therefore, it relies on reading these correct configurations in the snapshot.
     */
    public void checkMountPaths(Config config) {
        String rootMount = Utility.getSubvolFromProcMounts("/");
        if (rootMount != null && !rootMount.equals(config.rootSubvolumePath())) {
            String errMessage = "ROOT_SUBVOLUME_PATH=\"" + config.rootSubvolumePath() + "\" doesn't match the expected path \"" + rootMount + "\" from /proc/mounts";
            Utility.exitWithError(errMessage);
        }

        String snapMount = Utility.getSubvolFromProcMounts("/.snapshots");
        if (snapMount != null && !snapMount.equals(config.rootSnapshotsPath())) {
            //snapshots in custom snapper layout
            String errMessage = "ROOT_SNAPSHOTS_PATH=\"" + config.rootSnapshotsPath() + "\" doesn't match the expected path \"" + snapMount + "\" from /proc/mounts";
            Utility.exitWithError(errMessage);
        } else if (snapMount == null && !(config.rootSubvolumePath() + "/.snapshots").equals(config.rootSnapshotsPath())) {
            // Nested snapshots in default snapper layout
            String errMessage = "ROOT_SNAPSHOTS_PATH=\"" + config.rootSnapshotsPath() + "\" doesn't match the expected path \"" + config.rootSubvolumePath() + "/.snapshots\"";
            Utility.exitWithError(errMessage);
        }
    }
}

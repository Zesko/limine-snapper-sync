# Limine-Snapper-Sync

### Contents

- [Description](#description)
- [Screenshots](#screenshots)
- [Requirements](#requirements)
- [Installation](#installation)
- [Boot Configuration](#boot-configuration)
- [Tool Configuration](#tool-configuration)
- [Implementation](#implementation)
- [Donate](#donate)
- [Planned Version 2.0.0](#donate)

## Description

The tool syncs Limine boot entries with Snapper snapshots (Btrfs snapshots).

**What it does:**

* **Adding Snapshots to Limine:**
  When a Snapper snapshot is created, it adds a matching boot entry with the correct kernel versions to Limine.

* **Deleting Snapshots:**
  If a Snapper snapshot is deleted, the matching boot entry and kernel files are also removed.

* **Restoring Snapshot:**
  The tool supports `rsync` or `btrfs` to restore a selected snapshot with matching kernel versions.

* **Backup Entries:**
  After restoring, a "backup" entry is added, allowing you to revert to it or remove it later.

* **Boot Files Repair:**
  If corrupt bootable snapshot files are detected on the ESP,
  the tool automatically fixes them when creating a snapshot with the same file.
  Additionally, it logs hardware issues if two hashes of the same bootable file don’t match.

This tool supports custom Snapper layouts but not arbitrary Btrfs layouts, which is why it’s named
"limine-**snapper**-sync" instead of "limine-**btrfs**-sync".

It creates a folder at `/<ESP path>/<Machine-ID>/limine_history/` track snapshots related to kernel versions on the ESP.
Don’t change these files manually!

#### There are five available commands, each without any options:

* `limine-snapper-sync` syncs the Limine snapshot entries with the Snapper list, similar to how `grub-btrfs` works for
  Grub.
* `limine-snapper-list` shows the Limine snapshot list.
* `limine-snapper-info` shows details about versions, the total number of bootable snapshots, kernel file verification.
* `limine-snapper-restore` restores a selected snapshot to your system.
* `limine-snapper-watcher` should be executed **once** at startup. It ensures the Limine snapshot list is updated
  immediately when changes to the Snapper list is detected, functioning as an auto-sync.

**Note:** This tool is **not** for installing kernels or creating boot entries when you install a new kernel.
If you need that, you can use [limine-entry-tool](https://gitlab.com/Zesko/limine-entry-tool),
which helps create or remove boot entries.
For Arch Linux, you can use the `limine-dracut-support` package from AUR.
It works with `dracut` and `pacman` hook to automatically handle kernel updates
and boot entries for the Limine bootloader, just like `mkinitcpio` and `grub-mkconfig`.

#### Advantages of Limine with this tool compared to Grub with grub-btrfs

* **Faster boot with encrypted partitions:**
    - Bootable files (Initramfs/initrd and vmlinuz or UKI) are stored outside the fully encrypted partition.
      This allows for **faster decryption** compared to Grub, which uses its own decryption method.
    - Limine allows the initramfs/initrd with the Plymouth module to display a **Plymouth-themed password prompt UI**
      for unlocking the encrypted partition during boot.
      In contrast, Grub only provides a plain text-based password prompt on a black screen, lacking a graphical UI.
* **Easy multiple boot support:**
    - Each Linux distro can have its own snapshot entries in a separate Limine submenu.
      By contrast, Grub generates snapshot entries only for a single OS.
* **Flexibility and efficiency:**
    - Limine does **not** include **third-party drivers**
      (such as filesystems, LUKS2, various encryption methods, LVM, etc.).
      Instead, the kernel, initramfs or UKI handle these, making the boot process more efficient.
      This is different from Grub, which includes some drivers that might cause compatibility issues with future
      filesystem or kernel updates.
* **Faster snapshot updates:**
    - This tool updates the snapshot list faster than Grub, especially on NVMe SSDs,
      as it doesn't scan all subvolumes/snapshots.

#### Disadvantages of Limine with this tool compared to Grub with grub-btrfs

* **Old snapshots before installing the tool**:
    - Snapshots created before installing this tool won’t be added to the Limine bootloader because they’re missing
      their kernel files.
* **Limited Btrfs layout support:**
    - The tool works only with default or custom Snapper layouts.
      Grub-btrfs supports Snapper, Timeshift, and any Btrfs layout.
* **"ESP usage" (Neutral):**
    - In version 1.y.x (earlier than 2.0.0), a larger ESP is required since bootable snapshot files are stored there.
      However, the tool uses deduplication to save space.
    - **Note:** A planned major version(2.0.0) will fully support ESP sizes below 100 MiB
      by moving all bootable snapshot files to a separate FAT32 partition.
      (This version has been successfully tested but is not yet released.)

## Screenshots:

Choose between two different Limine menu layouts:

### Snapshots menu inside OS entry

The "Snapshots" menu is integrated within the OS entry.
![image](screenshots/1.jpg)

Expand it to view snapshot details such as **ID**, **date** and **description**.
Each Linux distro can have its own separate snapshot list.

![image](screenshots/2.jpg)

### Snapshots menu outside the OS entry

The "Snapshots" menu is listed separately from the OS entry.
![image](screenshots/7.jpg)

After booting from a snapshot, a notification appears, allowing one-click restoration.

![image](screenshots/3.jpg)

Clicking "Restore now" provides more details about the restore process.

![image](screenshots/4.jpg)

If a display manager (e.g., GDM) fails on a read-only snapshot, switch to TTY and run: `limine-snapper-restore`

Run `limine-snapper-info` to verify bootable snapshot files.

![image](screenshots/6.jpg)

## Requirements

**Note:** It supports Limine major version **8 or newer** because the configuration syntax of Limine 8 has
changed significantly compared to older versions of Limine.

* The ESP size should be at least 1 GiB, depending on how many Kernel versions you intend to install.
* Ensure Limine version **8.0.1 or newer** is installed.
* Install `snapper` and create a Snapper config name of your root subvolume.
* Install `btrfs-progs`
* Install `inotify-tools` to monitor when snapshots are created or deleted.
* Standard GNU coreutils: `cp`, `mv`, `rm`, `sha256sum`
* Standard util-linux: `logger`, `mount` and `umount`
* Java runtime version 17 or newer is required.
* Install `maven` for building and compiling source code.

## Installation:

### Arch Linux:

##### For `dracut`and EFI users (Easy setup)

1. Make sure `snapper` has a config name for your root
   subvolume. [Arch Wiki:Snapper](https://wiki.archlinux.org/title/Snapper)
1. Install [limine-dracut-support](https://aur.archlinux.org/packages/limine-dracut-support)
   and [limine-snapper-sync](https://aur.archlinux.org/packages/limine-snapper-sync/) from AUR
1. Run `bootctl --print-esp-path` to check if an ESP path is detected:
    * If detected, no further configuration is needed.
    * If not detected, edit `/etc/default/limine`
      to set `ESP_PATH=` to the correct ESP path.
      Then run `limine-update` to check if it was successful.
1. Enable the service to start syncing automatically:

```bash
systemctl enable --now limine-snapper-watcher.service
```

(Optional) Install `journalctl-desktop-notification` from AUR for desktop notifications about full ESP or a hardware
issues is detected.

------

##### For `mkinitcpio` or another initramfs tools like `booster` (Manuall setup)

1. Make sure `snapper` has a config name for your root subvolume.
1. Confirm Limine can boot your system by following
   the [Arch Wiki:Limine](https://wiki.archlinux.org/title/Limine#Limine-Snapper-Sync_setup).
1. Properly configure `limine.conf` to work with this tool. See [Boot Configuration](#Boot-Configuration) for details.
1. Install [limine-snapper-sync](https://aur.archlinux.org/packages/limine-snapper-sync/) from AUR.
1. Run `bootctl --print-esp-path` to check if an ESP path is detected:
    * If not detected, edit `/etc/default/limine` and set `ESP_PATH=` to the correct ESP path.
1. Create a root snapshot using `snapper`, then test `limine-snapper-sync` to see if
   `Saved successfully: <ESP>/limine.conf`.
1. If it works, enable the service to sync automatically:

```bash
systemctl enable --now limine-snapper-watcher.service
```

(Optional) Install `journalctl-desktop-notification` from AUR, it will notify you when ESP usage is full or a hardware
issue is detected.

------

### Linux distributions:

This tool should work on any Linux distribution, with or without systemd.
However, there are no pre-built packages for various package managers.
Package maintainers are welcome to create them.

Manual installation is required:

#### **For systemd distros:**

**Note:** This setup is not suitable for NixOS as it does not
follow [FHS](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).

* Ensure that Limine can boot your system.
* Clone the repository:

```shell
git clone https://gitlab.com/Zesko/limine-snapper-sync.git
cd limine-snapper-sync
```

* Build the project

```shell
mvn clean package
```

* Install the necessary files:

```shell
cp target/limine-snapper-sync.jar /usr/share/java/
cp install/arch-linux/usr/bin/* /usr/local/bin/
cp -r install/arch-linux/etc/* /etc/
cp -r install/arch-linux/usr/lib/systemd/* /etc/systemd/
```

* Install `libnotify` or [dunst](https://github.com/dunst-project/dunst) for desktop notifications on X11 or Wayland.
* Properly configure `limine.conf` to work with this tool. See [Boot Configuration](#Boot-Configuration) for details.
* Test to run `limine-snapper-sync`. If successful, you should see: `Saved successfully: <ESP>/limine.conf`.
* Enable the service to start syncing automatically:

```shell
systemctl enable --now limine-snapper-watcher.service
```

#### **For non-systemd distros:**

* Ensure that Limine can boot your system.
* Clone the repository:

```shell
git clone https://gitlab.com/Zesko/limine-snapper-sync.git
cd limine-snapper-sync
```

* Build the project

```shell
mvn clean package
```

* Install the necessary files:

```shell
cp target/limine-snapper-sync.jar /usr/share/java/
cp install/arch-linux/usr/bin/* /usr/local/bin/
cp install/arch-linux/etc/xdg/autostart/*.desktop /etc/xdg/autostart/` # Adjust based on ${XDG_CONFIG_DIRS}
cp install/arch-linux/etc/*.conf /etc/
````

* Install `libnotify` or [dunst](https://github.com/dunst-project/dunst) for desktop notifications on X11 or Wayland.
* Properly configure `limine.conf` to work with this tool. See [Boot Configuration](#Boot-Configuration) for details.
* Test to run `limine-snapper-sync`. If successful, you should see: `Saved successfully: <ESP>/limine.conf`.
* Create your own service or script to run `/usr/local/bin/limine-snapper-watcher` on every reboot.

## Boot Configuration:

**Note:** If you installed `limine-dracut-support` or `limine-entry-tool`, configuration is automated, and no manual setup is needed. 

### Manual Limine configuration: 
**Edit `/<ESP>/limine.conf`, here are some steps:**

1. **Add a kernel entry:**

   You **must** add at least one kernel entry in the format `//<Kernel name>` (e.g., `//Linux LTS`)
   within an OS entry block `/<OS name>`.

2. **Choose one snapshot layout:** (Optional)

    * Default: Add `//Snapshots` inside the OS entry block.

   **or**

    * Alternative: Add `/Snapshots` outside the OS entry block.

The keyword (`//Snapshots` or `/Snapshots`) tells the tool where snapshot entries should be placed.

**Note:** If neither keywords is found, `//Snapshots` will be automatically created when running
`limine-snapper-sync`.

#### Example 1:

If bootable kernel files are stored in `/efi/`, `/boot/`, `/boot/efi/`, or any other path used as the ESP
path:

```
/Arch Linux

  //Kernel LTS
  protocol: linux
  module_path: boot():/initramfs-linux-lts.img
  kernel_path: boot():/vmlinuz-linux-lts
  kernel_cmdline: rw rootflags=subvol=/@ root=UUID=068589c1-d9be-407c-ad86-101aa0912825
  
  //Kernel
  protocol: linux
  module_path: boot():/initramfs-linux.img
  kernel_path: boot():/vmlinuz-linux
  kernel_cmdline: rw rootflags=subvol=/@ root=UUID=068589c1-d9be-407c-ad86-101aa0912825

  //Snapshots
```

#### Example 2:

If bootable kernel files are stored in `/efi/d647abee5e264f469d7a38b751d9d1db/kernel-version/`
like where systemd-boot (`kernel-install`) stores them:

```
/+Arch Linux
comment: Any comment
comment: machine-id=d647abee5e264f469d7a38b751d9d1db

  //Mainline kernel
  protocol: linux
  module_path: boot():/d647abee5e264f469d7a38b751d9d1db/kernel-version/initrd
  kernel_path: boot():/d647abee5e264f469d7a38b751d9d1db/kernel-version/linux
  kernel_cmdline: rw rootflags=subvol=/@ root=UUID=068589c1-d9be-407c-ad86-101aa0912825

  //Mainline kernel fallback
  protocol: linux
  module_path: boot():/d647abee5e264f469d7a38b751d9d1db/kernel-version/initrd-fallback
  kernel_path: boot():/d647abee5e264f469d7a38b751d9d1db/kernel-version/linux
  kernel_cmdline: rw rootflags=subvol=/@ root=UUID=068589c1-d9be-407c-ad86-101aa0912825

/Snapshots
```

**Note:** If your ESP and system are on separate disks, use `guid(<Disk partition ID>):` instead of `boot():`.

**Warning:** After a kernel update, your distro's initramfs/initrd tool might change the kernel filepath,
causing a mismatch with `/<ESP path>/limine.conf` and resulting in a boot failure.
You should manually update `limine.conf` after every kernel update or use **one of the solutions** below:

* **Write a script** using `limine-entry-tool` to automatically create, update, or delete kernel entries in
  `limine.conf`.

**or** 

* **For Arch Linux**, `mkinitcpio`, `linux` and `linux-lts` packages keep kernel file paths unchanged 
  after every update by default, so `limine-entry-tool` is not needed.

**or**

* **Arch Linux with dracut**, install the `limine-dracut-support` package
  from [AUR](https://aur.archlinux.org/packages/limine-dracut-support) to automate kernel installation/removal.
______

## Tool Configuration

The config file is located at `/etc/default/limine` (**Recommend**) or alternatively at `/etc/limine-snapper-sync.conf`

**Note:**

- `/etc/default/limine` takes precedence over `/etc/limine-snapper-sync.conf`.
- When booting into a read-only snapshot where `/etc/*` cannot be modified,
`/etc/default/limine` is automatically copied to the temporary location `/tmp/limine`,
allowing temporary modifications.

Copy this to `/etc/default/limine` and adjust the settings as needed.

```properties
### OS Entry Targeting:
### The tool does not automatically know the custom OS name used in /<ESP>/limine.conf, which can contain multiple OS names for multi-boot scenarios.
### You need to specify the corresponding OS name here to match the name used in /<ESP>/limine.conf.
### Alternatively, you can add "comment: machine-id=<machine-id>" to your OS entry block in /<ESP>/limine.conf. 
### The machine-ID helps the tool to automatically target the correct OS entry. Changing the OS name does not matter.
TARGET_OS_NAME="Arch Linux"


### Max Snapshot Entries:
### Set the maximum number of snapshot entries for your OS. This depends on the ESP size and how many kernels will be installed.
### Note: The tool uses a deduplication function to prevent copying kernel versions that already exist.
MAX_SNAPSHOT_ENTRIES=8


### ESP Usage Limit:
### Set a usage limit for your ESP as a percentage between 1 and 99. The default value is 80. 
### The tool will stop creating new snapshot entries when this limit is reached.
### Tip for Arch Linux users: Install "journalctl-desktop-notification" from AUR, it will notify you when the usage limit is exceeded.
LIMIT_USAGE_PERCENT=80


### ESP Path:
### Specify the mount path of your ESP.
### Note: If you use systemd, you do not need to set this, as systemd (bootctl) will automatically detect the ESP path.
### However, specifying the ESP path manually can improve performance and reliability.
ESP_PATH="/boot"


### Snapper Configuration Name:
### Specify the Snapper configuration name. If not set, the tool will automatically detect it from "snapper list-configs".
### Specifying it manually improves performance by avoiding automatic detection overhead.
SNAPPER_CONFIG_NAME="root"


### Root Subvolume Path:
### Set the path to your root subvolume. The default value for some distros is "/@".
ROOT_SUBVOLUME_PATH="/@"


### Root Snapshot Path:
### Specify the path to your root snapshots. The default is "/@/.snapshots", which is used for the default Snapper layout.
### Note: The tool supports Snapper with any custom layout within the same filesystem, but not random Btrfs layouts without Snapper.
ROOT_SNAPSHOTS_PATH="/@/.snapshots"


### Restore Method Selection:
### Choose whether to enable rsync for restoration. If enabled, the tool will prompt you to choose between rsync or btrfs during the restore process.
### Advantage of rsync: The subvolume ID remains unchanged without "messing up" the Btrfs metadata.
### Disadvantage of rsync: Slower compared to Btrfs, which directly creates a new subvolume from the selected snapshot.
### "yes": Enables the prompt for choosing rsync or btrfs.
### "no":  Disables the prompt, defaulting to btrfs for "one-click-restore."
ENABLE_RSYNC_ASK=no


### Btrfs UUID:
### Specify the UUID of your Btrfs filesystem, used only during the restore process.
### If not set, the tool automatically determines the UUID during initialization and saves it in 'snapshots.json'. 
### If the saved UUID becomes outdated, set it manually.
#UUID=


### Authentication Method for Restore:
### Specify an authentication method for restore process. Options include 'sudo', 'doas', 'pkexec', `run0` or another method of your choice.
### If not set, 'pkexec' is used for Wayland/X11 environments, and 'sudo' for TTY.
#AUTH_METHOD=sudo


### Specify a terminal app and its argument to open the console UI for running the restore process with details.
### For example, "konsole -e" for KDE or "gnome-terminal -- bash -c" for GNOME.
#TERMINAL=konsole
#TERMINAL_ARG="-e"


### Snapshot Entry Formatting:
### Set the number of spaces to indent each line in all snapshot entries within /<ESP>/limine.conf.
SPACE_NUMBER=5


### Snapshot Name Format:
### Choose the format for how snapshot entries look in the Limine bootloader:
### 0. ID=111 2023-12-20 10:59:59 (default)
### 1. 111│2023-12-20 10:59:59
### 2. 111 │ 2023-12-20 10:59:59
### 3. 2023-12-20 10:59:59│111
### 4. 2023-12-20 10:59:59 │ 111
### 5. 2023-12-20 10:59:59
### 6. 111
SNAPSHOT_FORMAT_CHOICE=2


### Hash Function for Deduplication and Checksum:
### Select a hash function to avoid copying duplicate boot files. Options are "blake3", "sha1", or "sha256".
### WARNING: Changing the hash function will not affect files named with the previous hash function, potentially increasing duplication.
### Note: "blake3" must be installed manually if chosen.
HASH_FUNCTION=sha256


### Notification Icon:
### Set a custom icon to display in notifications when booting into a read-only snapshot and prompting for restoration.
NOTIFICATION_ICON="/usr/share/icons/hicolor/128x128/apps/LimineSnapperSync.png"


### Automatic Config Backup:
### Create a backup of 'limine.conf' and 'snapshots.json' before saving changes if they are older than the specified time (in hours).
### If not set, the default threshold is 8 hours.
BACKUP_THRESHOLD=8


### Commands Before Save
### Run custom commands before saving 'limine.conf'.
### If using multiple commands, separate them with '&&' or '||'.
### Note: Use single quotes (') for command argument when required, not double quotes (") as escape characters.
### Example: 'limine-reset-enroll' command is provided by 'limine-dracut-support' or 'limine-entry-tool' package.
COMMANDS_BEFORE_SAVE="limine-reset-enroll"


### Commands After Save
### Run custom commands after saving 'limine.conf'.
### If using multiple commands, separate them with '&&' or '||'.
### Note: Use single quotes (') for command argument when required, not double quotes (") as escape characters.
### Example: 'limine-enroll-config' command is provided by 'limine-dracut-support' or 'limine-entry-tool' package.
COMMANDS_AFTER_SAVE="limine-enroll-config"

```

## Implementation:

#### First Start

* **Machine ID Generation:** If `/etc/machine-id` does not exist, the tool generates one for unique OS identification,
  useful for multi-boot setups.
* **Configuration Verification:** The tool checks your Btrfs layout and config. If they are invalid, it will not start.
* **Config Restore:** If a `snapshots.json` file becomes corrupted, the tool automatically restores it.
* **Limine Entries:** The tool reads all entries from the Limine menu and converts them into a tree
  structure for easier management.
* **Mutex Lock:** Only one instance of the tool runs at a time to avoid conflicts.

#### Creating or Deleting Snapshots

* **Snapshot Monitoring:** The tool uses `inotifywait` to monitor the `/.snapshots/` directory for changes (create or
  delete)
* **Snapshot Creation:** When a new Snapper snapshot is created, the tool copies the current OS boot entry and
  kernel versions into a new snapshot entry.
* **Deduplication**: The tool checks for existing kernel files in snapshots and avoids copying files that are already
  present.
* **History File:** A history file, `snapshots.json`, is updated with details about which kernel files belong to which
  snapshot.
* **Snapshot List Update:** The tool updates the snapshot list in `/<ESP path>/limine.conf`.
* **Snapshot Deletion:** When snapshots are deleted from the Snapper list, the tool removes the related
  snapshot entries and bootable files.
  (unless needed by other snapshots).
* **Bootable File Repair:** If a bootable snapshot file is corrupted on ESP, the tool replaces it with a healthy version from
  creating the new snapshot.

#### Restore Process

* **Restore Methods:** The tool provides two restore methods: `rsync` or `btrfs`.
  By default, `rsync` is disabled.
  To enable `rsync`, set `ENABLE_RSYNC_ASK=yes` in `/etc/default/limine`.
* **Pre-Restore Check:** The tool checks Btrfs layout and config before restoring.
  If not valid, the restore won't proceed.
* **Btrfs Restore:** The tool creates a new subvolume from the selected snapshot and moves subvolumes to it.
* **Rsync Restore:** With `rsync`, the tool excludes other mount points and child subvolumes,
  copying files from the snapshot and deleting files not in the snapshot.
* **Backup Handling:** The tool moves the current kernel files
  and adds the "backup" subvolume to the Snapper list for easy restoration or deletion.
* **Kernel Files:** The bootable kernel files are restored from the history directory to the correct location.
* **Boot Entry Restore:** The tool restores the corresponding boot config from `snapshots.json` to the OS entry in
  `limine.conf`.

______________________

## Donate

If you would like to support my work on the planned version 2.0.0,
a complete rewrite in Kotlin with some advantages and improvements over version 1, feel free to donate:

[<img src="https://gitlab.com/Zesko/resources/-/raw/main/kofiButton.png" width=110px>](https://ko-fi.com/zeskotron)
&nbsp;[<img src="https://gitlab.com/Zesko/resources/-/raw/main/PayPalButton.png" width=160px/>](https://www.paypal.com/donate/?hosted_button_id=XKP36D62AY5HY)


#### Planned version 2.0.0

- **Support for smaller ESP sizes** by moving bootable snapshot files to a separate FAT32 partition.

- **`skip-kernel-snapshot` option:** Excludes specific kernel entries from snapshots by adding this option to their comments.

- **Btrfs layout changes for Snapper:** All old cmdlines stored in metadata will be updated automatically.

- **limine-snapper-remove:** Allows removing selected boot files to free up space on a boot partition.

- **Bcachefs support** will be added once Snapper is ready - see [TODOs](https://github.com/openSUSE/snapper/blob/master/doc/bcachefs.txt).

- **Complete kotlin rewrite:** for a cleaner and more efficient codebase compared to version 1.

- **Improving performance** with optimized Kotlin code and standard libraries.

- **Faster build** by switching from Maven to Gradle.

- **Smaller binary size** than version 1.

- **Full compatibility** with version 1.
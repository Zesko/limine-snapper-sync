# Changelog

## 1.8.3 - 2025-03-08

### Fixed or improved

* Enhanced protection against accidental deletion of all snapshot entries due to user error.
* Made replacing kernel cmdline with its snapshotted cmdline more reliable to ignore misconfiguration of user.
* Improved verification of `ROOT_SUBVOLUME_PATH` and `ROOT_SNAPSHOTS_PATH` before creating a snapshot.
  This ensures correct configuration when restoring from a snapshot.
* Reduced message code and removed some unnecessary code.
* Prevented any terminal GUI from closing automatically in case of a restore failure.

## 1.8.2 - 2025-03-06

### Fixed

* A broken cmdline will be forcibly replaced with a valid cmdline during restore to create a "backup" entry and prevent
  interruption.
* Keep any terminal GUI open on restore failure instead of closing automatically.
* Skip `reboot` command if it is not available after restore.
* Prompt for OS name if not found before starting restore.
* Allow updating `snapshots.json` when an ESP usage limit is exceeded, but do not update the last Snapper ID.

## 1.8.1 - 2025-03-01

### Improved

* Removed duplicate and unnecessary code.
* Renamed some variables and methods for better readability.
* Use Java's native library and Bash utilities instead of `grep`, `touch`, `stat` and `mkdir`.

### Fixed

- Fixed multiple commands running before or after saving `limine.conf`.
- Fixed the default icon for `libnotify` notification.
- No longer adds a snapshot entry if no snapshotted kernel entry exists in snapshots.json.

## 1.8.0 - 2025-02-23

### Added

- Support for restoring snapshots in a chroot environment.
- Automatically stop snapshot restore if the predicted ESP usage is expected to be fully exhausted.

### Changed

- `LIMIT_USAGE_PERCENT` now defaults to 80 if not set.
- Improved ESP usage check before creating a snapshot.

## 1.7.1 - 2025-02-17

### Fixed

- **limine-snapper-info** now properly supports Limine version 9.

## 1.7.0 - 2025-02-16

### Added

- Added support for `path:` in `limine.conf` (Limine version 8.7.1 or newer)

### Changed

- Allow copying of any Limine key(e.g.,`resolution`) into new snapshot entry during snapshot creation.
- Converted hardcoded enums to flexible strings for JSON (de)serialization to avoid compatibility issues during upgrades
  or downgrades.
- Replaced `LIMIT_NUMBER` with `MAX_SNAPSHOT_ENTRIES`.

### Removed

- Removed the first hardcoded Limine key from each detail line in the JSON file.

## 1.6.0 - 2025-01-17

### Added

- Automatically initialize a Snapper config for `/` if one is not already present.
- Automatically create a root snapshot after initializing the Snapper config.

### Changed

- Replaced hardcoded version config with dynamic version retrieval.
- Reformatted `pom.xml` file.

## 1.5.2 - 2024-12-31

### Fixed

- No need to create a backup of snapshots.json if it does not exist.
- Improved language clarity for the configuration error message.

## 1.5.1 - 2024-12-27

### Fixed

- Renamed `BACKUP_INTERVAL` to `BACKUP_THRESHOLD` for better clarity.
- Clarified configuration descriptions for `COMMANDS_BEFORE_SAVE` and `COMMANDS_AFTER_SAVE`.

## 1.5.0 - 2024-12-24

### Added

- Support for reading `/etc/default/limine.conf`, which overrides the existing `/etc/limine-snapper-sync.conf`.
- New config option `COMMANDS_BEFORE_SAVE` for running custom commands before saving `limine.conf`.
- New config option `COMMANDS_AFTER_SAVE` for running custom commands after saving `limine.conf`.

### Changed

- Refactored `ConfigReader` to support reading from multiple configuration files.

### Fixed

- Fixed a typo in the machine-ID regex.

## 1.4.0 - 2024-12-18

### Added

- Support for reading options in any boot entry comments.
  These options are compatible with `limine-entry-tool 1.7.0` or newer.

### Changed

- Improved the algorithm for identifying machine-ID in boot entry comments.

## 1.3.0 - 2024-12-15

### Added

- Support for using `notify-send` as an alternative to `dunst`.
- A new logo for use as the notification icon.
- A `limine-snapper-restore.desktop` app has been added.
- A method to validate names as valid file names on FAT32.
- A method to handle case-sensitive paths on FAT32.

### Changed

- Switched from static backups to time-based backups.
- Refactored variable names for clarity.
- Simplified and adjusted the README.
- Automatically create a default `//Snapshots` entry in limine.conf if both `//Snapshots` and `/Snapshots` are missing.

### Fixed

- Resolved a permission issue that prevented restore functionality when `sudo` or `pkexec` was not used.

## 1.2.0 - 2024-12-10

### Changed

- Improved the output details of the executable command method in the Java code.
- Simplified the restoration bash script by reducing its code.

### Added

- Introduced two new methods to validate EFI and BIOS Legacy modes.
- Added `TERMINAL_ARG` to the configuration file.
- Added additional default terminal for restoration, depending on different desktop environment,
  if `TERMINAL` is not set in the config.

## 1.1.2 - 2024-11-24

### Fixed

- During a restore, 'bad' bootable files should not be deleted too early; they should first be moved to the Limine
  backup if not already present.

## 1.1.1 - 2024-11-10

### Removed

- Remove the hardcoded version in the executable JAR file name to simplify use in some scripts.

## 1.1.0 - 2024-10-03

### Added

- Users can now choose a custom format for snapshot names as they appear in Limine bootloader.
- Added support for the Blake3 hash function, which is faster than SHA256.
- Automatic backups of `snapshots.json` and `limine.conf` are created before saving.
- Automatically load `snapshots.json.old` as a backup if `snapshots.json` is corrupted.
- Using two processes to calculate two hashes of the same file to better detect corruption/hardware issues.
- Damaged bootable files are now automatically replaced with healthy files when creating a new snapshot.

### Changed

- README: Installing `dunst` for desktop notification of one-click restoration is now optional, not mandatory.

## 1.0.0 - 2024-08-20

### Added

- Limine-Snapper-Sync is finally created
- Changelog
- License
#!/usr/bin/env bash

LOCKFILE="/tmp/limine-snapper-sync.lock"

sleep 1

# Check if the lockfile exists and wait until it is removed
while [ -f "$LOCKFILE" ]; do
    echo " Waiting for limine-snapper-sync to finish..."
    sleep 1
done

exit 0

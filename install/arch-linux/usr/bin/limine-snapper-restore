#!/usr/bin/env bash

# Load configuration
if [[ -f /etc/limine-snapper-sync.conf ]]; then
    # shellcheck disable=SC1091
    source /etc/limine-snapper-sync.conf
fi

if [[ -f /etc/default/limine ]]; then
    # shellcheck disable=SC1091
    source /etc/default/limine
else
    echo "Warning: Default Limine configuration /etc/default/limine not found." >&2
fi

# Variables
CONFIG_TMP="/tmp/limine"
LOCKFILE="/tmp/limine-snapper-restore.lock"
RESTORE_CMD='java -jar /usr/share/java/limine-snapper-sync.jar --restore'
TITLE="Restore this snapshot now!"
MESSAGE="You are currently using this snapshot. Please restore it before rebooting to the normal system."

# Check if the snapshot is read-only and load temporary config
check_and_load_config() {
    # Read the Btrfs property "ro" from the root partition
    local prop
    prop=$(btrfs property get / ro 2>/dev/null)
    # Check if "true" is included in the result
    if [[ $prop == *true* ]]; then
        if [[ -f $CONFIG_TMP ]]; then
            # Load the temporary config if it exists
            # shellcheck disable=SC1090
            source "$CONFIG_TMP"
        elif [[ -f /etc/default/limine ]]; then
            cp /etc/default/limine "$CONFIG_TMP"
        fi
    fi
}

# Cleanup lockfile on exit
cleanup() {
    rm -f "$LOCKFILE"
}

trap cleanup EXIT

# Check if you are in snapshot
is_snapshot() {
    cmdline=$(< /proc/cmdline)
    if [[ $cmdline =~ rootflags.*subvol=.*?/([0-9]+)/snapshot ]]; then
        return 0
    else
        return 1
    fi
}

# Check if you are running in root
is_root() {
    [[ $EUID -eq 0 ]]
}

# Function to check if the script is running in a graphical environment
is_graphical() {
    [[ $XDG_SESSION_TYPE == "x11" || $XDG_SESSION_TYPE == "wayland" ]]
}

get_terminal_command() {
    if [[ -n "$TERMINAL" ]] && command -v "$TERMINAL" &>/dev/null; then
        # Set a default TERMINAL_ARG if it's not specified
        case "$TERMINAL" in
        gnome-terminal)
            echo "$TERMINAL ${TERMINAL_ARG:--- bash -c}"
            ;;
        *)
            echo "$TERMINAL ${TERMINAL_ARG:--e}"
            ;;
        esac
    elif command -v konsole &>/dev/null; then
        echo "konsole -e"
    elif command -v gnome-terminal &>/dev/null; then
        echo "gnome-terminal -- bash -c"
    elif command -v xfce4-terminal &>/dev/null; then
        echo "xfce4-terminal -e"
    elif command -v qterminal &>/dev/null; then
        echo "qterminal -e"
    elif command -v mate-terminal &>/dev/null; then
        echo "mate-terminal -e"
    elif command -v kgx &>/dev/null; then
        echo "kgx -e"
    elif command -v deepin-terminal &>/dev/null; then
        echo "deepin-terminal -e"
    elif command -v xterm &>/dev/null; then
        echo "xterm -e"
    else
        return 1
    fi
}

# Notify using dunstify or notify-send
notify_user() {
    local action
    if command -v dunstify &>/dev/null; then
        action=$(dunstify -u "critical" --appname="Snapshot detected!" --icon="$NOTIFICATION_ICON" --action="default,Reply" --action="openRestore,Restore now" "$TITLE" "$MESSAGE")
    elif command -v notify-send &>/dev/null; then
        action=$(notify-send -u "critical" --app-name="Snapshot detected!" --icon="$NOTIFICATION_ICON" --action="default=Reply" --action="openRestore=Restore now" "$TITLE" "$MESSAGE")
    else
        echo -e "\033[91m notify-send or dunst is not installed.\033[0m"
        exit 1
    fi
    echo "$action"
}

# Main logic
check_and_load_config
# Create lock file
: >> "$LOCKFILE"

# Determine authentication method
if [[ -z $AUTH_METHOD ]] && ! is_root; then
    if is_graphical && command -v pkexec >/dev/null; then
        AUTH_METHOD="pkexec"
    elif command -v sudo >/dev/null; then
        AUTH_METHOD="sudo"
    else
        echo -e "\033[91m Root privileges are required.\033[0m"
        exit 1
    fi
fi

# Handle notification mode
if [[ $1 == "--notify" ]]; then
    if ! is_snapshot; then
        echo "You are not in a snapshot."
        exit 0
    fi
fi

# Notify and execute the restore command
if is_graphical && [[ $1 == "--notify" ]] && ! is_root; then
    action=$(notify_user)
    if [[ $action == "openRestore" || $action == "default" ]]; then
        terminal_cmd=$(get_terminal_command)
        if [[ -n $terminal_cmd ]]; then
            eval "$terminal_cmd \"$AUTH_METHOD $RESTORE_CMD\""
        else
            echo -e "\033[1;31m No suitable terminal found.\033[0m"
            exit 1
        fi
    fi
elif is_graphical && ! is_root; then
    terminal_cmd=$(get_terminal_command)
    if [[ -n $terminal_cmd ]]; then
        eval "$terminal_cmd \"$AUTH_METHOD $RESTORE_CMD\""
    else
        echo -e "\033[1;31m No suitable terminal found.\033[0m"
        exit 1
    fi
elif is_root; then
    bash -c "$RESTORE_CMD"
else
    # Fallback for TTY or non-graphical environments
    bash -c "$AUTH_METHOD $RESTORE_CMD"
fi
